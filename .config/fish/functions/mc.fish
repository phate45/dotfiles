# source: https://gist.github.com/halicki/58cedaf90f3e85277a799cef8217fc72#file-mc-wrapper-fish
# see also: https://wiki.archlinux.org/title/Midnight_Commander

function mc
    set SHELL_PID %self

    if not test -d "/tmp/mc-$USER"
        mkdir -p "/tmp/mc-$USER"
    end

    set MC_PWD_FILE "/tmp/mc-$USER/mc.pwd.$SHELL_PID"
        
    /usr/bin/mc -P $MC_PWD_FILE $argv
        
    if test -r $MC_PWD_FILE
    
        set MC_PWD (cat $MC_PWD_FILE)
        if test -n "$MC_PWD"
            and test -d "$MC_PWD"
            cd (cat $MC_PWD_FILE)
        end
        
        rm -f $MC_PWD_FILE
    end
end
