# variables

# disable default greeting
set -e fish_greeting
# set editor
set -x EDITOR nvim
# set manpager
# set -x MANPAGER "/bin/sh -c \"col -b | vim --not-a-term -c 'set ft=man ts=8 nomod nolist noma' -\""
set -x MANPAGER "/bin/sh -c 'col -bx | bat -l man -p'"
set -x MANROFFOPT "-c"
# disable systemd pager
set -x SYSTEMD_PAGER
# set some sensible default options to always pass into invocations of less
set -x LESS "--ignore-case --LONG-PROMPT --RAW-CONTROL-CHARS --tabs=4 --window=-4"
# don't store any history of commands executed in less
set -x LESSHISTFILE /dev/null


## aliases
alias clear='/bin/clear; echo; echo; seq 1 (tput cols) | sort -R | spark | lolcat; echo; echo'

# Changing "ls" to "exa"
alias ls='exa -al --color=always --group-directories-first' # my preferred listing
alias la='exa -a --color=always --group-directories-first'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first'  # long format
alias lt='exa -aT --color=always --group-directories-first' # tree listing
alias l.='exa -a | egrep "^\."'
alias lg='ls --grid'  # grid output

# adding flags
alias free='free -mt'  # show sizes in MB
alias df='df -h'
alias mkdir='mkdir -p'

#hardware info --short
alias hw="hwinfo --short"

# continue download
alias wget="wget -c"

# better grep (with smart case)
alias grep="rg -S"

# better find
alias find="fd"
alias fd="fd --follow"

# simplify
alias psgrep="ps aux | grep -v grep | grep -i -e VSZ -e"
alias hgrep="history | grep -i -e"

# set in stone
alias vim="nvim"

# better package manager
alias aura='sudo aura'

# recording gifs
alias ascii2gif="docker run --rm -v $PWD:/data asciinema/asciicast2gif -s 2"
alias ascii2gifnl="docker run --rm -v $PWD:/data --env GIFSICLE_OPTS=\"-k 64 -O2 -Okeep-empty --lossy=80 --no-loopcount\" asciinema/asciicast2gif -s 2"

#youtube-dl
alias yta-aac="youtube-dl --extract-audio --audio-format aac"
alias yta-best="youtube-dl --extract-audio --audio-format best"
alias yta-flac="youtube-dl --extract-audio --audio-format flac"
alias yta-m4a="youtube-dl --extract-audio --audio-format m4a"
alias yta-mp3="youtube-dl --extract-audio --audio-format mp3"
alias yta-opus="youtube-dl --extract-audio --audio-format opus"
alias yta-vorbis="youtube-dl --extract-audio --audio-format vorbis"
alias yta-wav="youtube-dl --extract-audio --audio-format wav"
alias ytv-best="youtube-dl -f bestvideo+bestaudio"

# confirm before overwriting something
alias cp="cp -i"
alias mv='mv -i'
alias rm='rm -i'

# better cat
alias cat="bat"

# get the error messages from journalctl
alias jctl="journalctl -p 3 -xb"

# git-branchless
alias gb="git-branchless wrap"

# ip command colored
alias ip="ip -color"

# Fish command history
function history
    builtin history --show-time='%F %T ' $argv
end

# Add ~/.local/bin to PATH
if test -d ~/.local/bin
    if not contains -- ~/.local/bin $PATH
        set -p PATH ~/.local/bin
    end
end

# Add doom emacs bin to PATH
if test -d ~/.config/emacs/bin
    if not contains -- ~/.config/emacs/bin $PATH
        set -p PATH ~/.config/emacs/bin
    end
end

# autojump integration
if test -f /usr/share/autojump/autojump.fish; . /usr/share/autojump/autojump.fish; end

### RANDOM COLOR SCRIPT ###
colorscript -r

# direnv
direnv hook fish | source

# prompt
starship init fish | source
