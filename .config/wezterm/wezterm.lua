local wezterm = require('wezterm')

-- source: https://github.com/joshmedeski/dotfiles/tree/main/.config/wezterm
local w = require('wallpaper')
local b = require('background')

return {
  color_scheme = "nord",
  window_background_opacity = 0.95,
  background = {
    w.get_wallpaper(),
    b.get_background(),
  },
  default_prog = {"/usr/bin/fish"},
  leader = { key="f", mods="CTRL", timeout_milliseconds=1000 },
  keys = {
    -- This will create a new split and run your default program inside it
    {key="-", mods="LEADER", action=wezterm.action{SplitHorizontal={domain="CurrentPaneDomain"}}},
    {key="=", mods="LEADER", action=wezterm.action{SplitVertical={domain="CurrentPaneDomain"}}},
    {key="w", mods="LEADER", action=wezterm.action.ReloadConfiguration},
  },
  check_for_updates = false,
  hide_tab_bar_if_only_one_tab = true,
  font = wezterm.font_with_fallback {
    { family = 'JetBrains Mono', weight = 'Medium' },
    { family = 'Terminus', weight = 'Bold' },
    'Noto Color Emoji',
  },
  enable_kitty_keyboard = true,
  warn_about_missing_glyphs = false,
}
