local M = {}
local h = require("helpers")

M.get_background = function()
	return {
		source = {
			Gradient = {
				-- #000000 changed to #2e3440 as per the nord color scheme
				colors = { h.is_dark() and "#2e3440" or "#ffffff" },
			},
		},
		width = "100%",
		height = "100%",
		opacity = h.is_dark() and 0.95 or 0.7,
	}
end

return M

