# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


import os
import re
import socket
import subprocess
from libqtile.config import Key, Screen, Group, Drag, Click, Rule, Match
from libqtile.config import ScratchPad, DropDown
from libqtile.lazy import lazy
from libqtile.backend.x11.window import Window
from libqtile.core.manager import Qtile
from libqtile import layout, bar, widget, hook
from libqtile import qtile as qtile_global

from phate_conf import colors, bar_colors, nord_colors, float_to_front, window_to_next_group, window_to_prev_group
from phate_conf import arrow_widget

## Constants

QTILE: Qtile = qtile_global

# mod4 or mod = super key
mod = "mod4"
mod1 = "mod1"  # alt
mod2 = "control"
home = os.path.expanduser('~')
qtile_config_folder = home + "/.config/qtile"
scripts_folder = qtile_config_folder + '/scripts/'
myShell = "fish"
# myTerm = "alacritty"
# myTermRun = myTerm + " -e "
# myTermPlusShell = myTermRun + myShell  # for alacritty
myTerm = "wezterm"
myTermRun = myTerm + " start --always-new-process -- "
myTermPlusShell =  myTermRun + myShell
myConfig = qtile_config_folder + "/config.py"

WINKEY = [mod]
WIN_ALT = [mod, mod1]
WIN_SHIFT = [mod, "shift"]
WIN_CTRL = [mod, mod2]

### KEYS ###

keys = [

## My keybindings

    # Terminal
    Key(WINKEY, "Return", lazy.spawn(myTermPlusShell), desc='Launches My Terminal'),
    Key(WINKEY, "KP_Enter", lazy.spawn(myTermPlusShell), desc='Launches My Terminal'),

    # Switch focus to specific monitor
    Key(WINKEY, "w", lazy.to_screen(0), desc='Keyboard focus to monitor 1'),
    Key(WINKEY, "e", lazy.to_screen(1), desc='Keyboard focus to monitor 2'),
    Key(WINKEY, "r", lazy.to_screen(2), desc='Keyboard focus to monitor 3'),
    Key(WINKEY, "t", lazy.to_screen(3), desc='Keyboard focus to monitor 4'),

    # Switch focus of monitors
    Key(WINKEY, "period", lazy.next_screen(), desc='Move focus to next monitor'),
    Key(WINKEY, "comma", lazy.prev_screen(), desc='Move focus to prev monitor'),

    # Kill focused window
    Key(WINKEY, "q", lazy.window.kill()),

    # Launcher
    Key(WIN_SHIFT, "Return", lazy.spawn('rofi -show run')),
    Key(WIN_SHIFT, "d", lazy.spawn("dmenu_run -i -nb '#191919' -nf '#fea63c' -sb '#fea63c' -sf '#191919' -fn 'NotoMonoRegular:bold:pixelsize=14'")),

    # Restart Qtile
    Key(WIN_SHIFT, "r", lazy.restart()),
    Key(WIN_SHIFT, "c", lazy.reload_config()),

    # Quick screen lock
    Key(WIN_SHIFT, "l", lazy.spawn('i3lock -k -c 08051add --time-color=305890 --date-color=123456'), desc="Quick screen lock"),

    # Window controls
    Key(WINKEY, "j", lazy.layout.down(), desc='Move focus down in current stack pane'),
    Key(WINKEY, "k", lazy.layout.up(), desc='Move focus up in current stack pane'),
    Key(WINKEY, "Up", lazy.layout.up()),  # alternates for ease of use
    Key(WINKEY, "Down", lazy.layout.down()),

    Key(WIN_SHIFT, "j", lazy.layout.shuffle_down(), desc='Move windows down in current stack'),
    Key(WIN_SHIFT, "k", lazy.layout.shuffle_up(), desc='Move windows up in current stack'),
    Key(WIN_SHIFT, "Down", lazy.layout.shuffle_down(), desc='Move windows down in current stack'),
    Key(WIN_SHIFT, "Up", lazy.layout.shuffle_up(), desc='Move windows up in current stack'),

    Key(WINKEY, "h", lazy.layout.grow(), lazy.layout.increase_nmaster(), desc='Expand window (MonadTall), increase number in master pane (Tile)'),
    Key(WINKEY, "l", lazy.layout.shrink(), lazy.layout.decrease_nmaster(), desc='Shrink window (MonadTall), decrease number in master pane (Tile)'),
    Key(WINKEY, "Left", lazy.layout.grow(), lazy.layout.increase_nmaster(), desc='Expand window (MonadTall), increase number in master pane (Tile)'),
    Key(WINKEY, "Right", lazy.layout.shrink(), lazy.layout.decrease_nmaster(), desc='Shrink window (MonadTall), decrease number in master pane (Tile)'),

    Key(WINKEY, "n", lazy.layout.normalize(), desc='normalize window size ratios'),
    Key(WINKEY, "m", lazy.layout.maximize(), desc='toggle window between minimum and maximum sizes'),

    Key(WIN_SHIFT, "f", lazy.window.toggle_floating(), desc='toggle floating'),
    Key(WIN_SHIFT, "m", lazy.window.toggle_fullscreen(), desc='toggle fullscreen'),

    ## Alt-Tab, anyone?
    # Key(WINKEY, "Tab", lazy.screen.next_group()),
    Key([mod1], "Tab", lazy.screen.next_group()),
    Key([mod1, "shift"], "Tab", lazy.screen.prev_group()),

    ## Treetab controls
    Key(WIN_CTRL, "k", lazy.layout.section_up(), desc='Move up a section in treetab'),
    Key(WIN_CTRL, "j", lazy.layout.section_down(),desc='Move down a section in treetab'),
    Key(WIN_CTRL, "Up", lazy.layout.section_up(), desc='Move up a section in treetab'),
    Key(WIN_CTRL, "Down", lazy.layout.section_down(),desc='Move down a section in treetab'),

    ## Stack controls
    Key(WIN_SHIFT, "space", lazy.layout.rotate(), lazy.layout.flip(), desc='Switch which side main pane occupies (MonadTall)'),
    Key(WINKEY, "space", lazy.layout.next(), desc='Switch window focus to other pane(s) of stack'),
    Key(WIN_CTRL, "Return", lazy.layout.toggle_split(), desc='Toggle between split and unsplit sides of stack'),

    ## Apps
    Key(WIN_ALT, "a", lazy.spawn('xfce4-appfinder')),
    Key(WIN_ALT, "c", lazy.spawn('chromium')),
    Key(WIN_ALT, "d", lazy.spawn('discord')),
    Key(WIN_ALT, "e", lazy.spawn('doom +everywhere')),
    Key(WIN_ALT, "f", lazy.spawn('firefox --ProfileManager')),
    Key(WIN_ALT, "m", lazy.spawn('xfce4-settings-manager')),
    Key(WIN_ALT, "n", lazy.spawn('nemo')),
    Key(WIN_ALT, "o", lazy.spawn('obs')),
    Key(WIN_ALT, "p", lazy.spawn('deadbeef')),
    Key(WIN_ALT, "s", lazy.spawn('rofi -show ssh')),
    Key(WIN_ALT, "v", lazy.spawn('pavucontrol')),
    # Key(WIN_ALT, "w", lazy.spawn(scripts_folder + 'mount_work_projects.sh')),

    ## Clipboard
    Key([mod1], "v", lazy.spawn("env CM_LAUNCHER=rofi clipmenu -lines 20 -columns 2 -p Clip: -i")),


# FUNCTION KEYS

    # Key([], "F12", lazy.spawn('xfce4-terminal --drop-down')),

# SUPER + FUNCTION KEYS

    # Key([mod], "e", lazy.spawn('atom')),
    # Key([mod], "c", lazy.spawn('conky-toggle')),
    # Key([mod], "f", lazy.window.toggle_fullscreen()),
    # Key([mod], "m", lazy.spawn('pragha')),
    # Key([mod], "q", lazy.window.kill()),
    # Key([mod], "r", lazy.spawn('rofi-theme-selector')),
    # Key([mod], "t", lazy.spawn('urxvt')),
    # Key([mod], "v", lazy.spawn('pavucontrol')),
    # Key([mod], "w", lazy.spawn('vivaldi-stable')),
    Key(WINKEY, "x", lazy.spawn('archlinux-logout')),
    Key(WINKEY, "Escape", lazy.spawn('xkill')),
    # Key([mod], "Return", lazy.spawn('termite')),
    # Key([mod], "KP_Enter", lazy.spawn('termite')),
    # Key([mod], "F1", lazy.spawn('vivaldi-stable')),
    # Key([mod], "F2", lazy.spawn('atom')),
    # Key([mod], "F3", lazy.spawn('inkscape')),
    # Key([mod], "F4", lazy.spawn('gimp')),
    # Key([mod], "F5", lazy.spawn('meld')),
    # Key([mod], "F6", lazy.spawn('vlc --video-on-top')),
    # Key([mod], "F7", lazy.spawn('virtualbox')),
    # Key([mod], "F8", lazy.spawn('thunar')),
    # Key([mod], "F9", lazy.spawn('evolution')),
    # Key([mod], "F10", lazy.spawn("spotify")),
    # Key([mod], "F11", lazy.spawn('rofi -show run -fullscreen')),
    # Key([mod], "F12", lazy.spawn('rofi -show run')),

# SUPER + SHIFT KEYS

    # Key([mod, "shift"], "Return", lazy.spawn('thunar')),
    # Key([mod, "shift"], "q", lazy.window.kill()),
    # Key([mod, "shift"], "r", lazy.restart()),
    # Key([mod, "control"], "r", lazy.restart()),
    # Key([mod, "shift"], "x", lazy.shutdown()),

# CONTROL + ALT KEYS

    # Key(["mod1", "control"], "Next", lazy.spawn('conky-rotate -n')),
    # Key(["mod1", "control"], "Prior", lazy.spawn('conky-rotate -p')),
    # Key(["mod1", "control"], "a", lazy.spawn('xfce4-appfinder')),
    # Key(["mod1", "control"], "b", lazy.spawn('thunar')),
    # Key(["mod1", "control"], "c", lazy.spawn('catfish')),
    # Key(["mod1", "control"], "e", lazy.spawn('arcolinux-tweak-tool')),
    # Key(["mod1", "control"], "f", lazy.spawn('firefox')),
    # Key(["mod1", "control"], "g", lazy.spawn('chromium -no-default-browser-check')),
    # Key(["mod1", "control"], "i", lazy.spawn('nitrogen')),
    # Key(["mod1", "control"], "k", lazy.spawn('arcolinux-logout')),
    # Key(["mod1", "control"], "l", lazy.spawn('arcolinux-logout')),
    # Key(["mod1", "control"], "m", lazy.spawn('xfce4-settings-manager')),
    # Key(["mod1", "control"], "o", lazy.spawn(home + '/.config/qtile/scripts/picom-toggle.sh')),
    # Key(["mod1", "control"], "p", lazy.spawn('pamac-manager')),
    # Key(["mod1", "control"], "r", lazy.spawn('rofi-theme-selector')),
    # Key(["mod1", "control"], "s", lazy.spawn('spotify')),
    # Key(["mod1", "control"], "t", lazy.spawn('termite')),
    # Key(["mod1", "control"], "u", lazy.spawn('pavucontrol')),
    # Key(["mod1", "control"], "v", lazy.spawn('vivaldi-stable')),
    # Key(["mod1", "control"], "w", lazy.spawn('arcolinux-welcome-app')),
    # Key(["mod1", "control"], "Return", lazy.spawn('termite')),

# ALT + ... KEYS

    # Key(["mod1"], "f", lazy.spawn('variety -f')),
    # Key(["mod1"], "h", lazy.spawn('urxvt -e htop')),
    # Key(["mod1"], "n", lazy.spawn('variety -n')),
    # Key(["mod1"], "p", lazy.spawn('variety -p')),
    # Key(["mod1"], "t", lazy.spawn('variety -t')),
    # Key(["mod1"], "Up", lazy.spawn('variety --pause')),
    # Key(["mod1"], "Down", lazy.spawn('variety --resume')),
    # Key(["mod1"], "Left", lazy.spawn('variety -p')),
    # Key(["mod1"], "Right", lazy.spawn('variety -n')),
    # Key(["mod1"], "F2", lazy.spawn('gmrun')),
    # Key(["mod1"], "F3", lazy.spawn('xfce4-appfinder')),

# VARIETY KEYS WITH PYWAL

    # Key(["mod1", "shift"], "f", lazy.spawn(home + '/.config/qtile/scripts/set-pywal.sh -f')),
    # Key(["mod1", "shift"], "p", lazy.spawn(home + '/.config/qtile/scripts/set-pywal.sh -p')),
    # Key(["mod1", "shift"], "n", lazy.spawn(home + '/.config/qtile/scripts/set-pywal.sh -n')),
    # Key(["mod1", "shift"], "u", lazy.spawn(home + '/.config/qtile/scripts/set-pywal.sh -u')),

# CONTROL + SHIFT KEYS

    Key([mod2, "shift"], "Escape", lazy.spawn('xfce4-taskmanager')),

# SCREENSHOTS

    Key([], "Print", lazy.spawn("scrot 'ArcoLinux-%Y-%m-%d-%s_screenshot_$wx$h.jpg' -e 'mv $f $$(xdg-user-dir PICTURES)'")),
    Key([mod2], "Print", lazy.spawn('flameshot gui')),
    Key([mod2, "shift"], "Print", lazy.spawn('gnome-screenshot -i')),

# MULTIMEDIA KEYS

# INCREASE/DECREASE BRIGHTNESS
    Key([], "XF86MonBrightnessUp", lazy.spawn("xbacklight -inc 5")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("xbacklight -dec 5")),

# INCREASE/DECREASE/MUTE VOLUME
    Key([], "XF86AudioMute", lazy.spawn("amixer -q set Master toggle")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -q set Master 5%-")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -q set Master 5%+")),

    Key([], "XF86AudioPlay", lazy.spawn("playerctl play-pause")),
    Key([], "XF86AudioNext", lazy.spawn("playerctl next")),
    Key([], "XF86AudioPrev", lazy.spawn("playerctl previous")),
    Key([], "XF86AudioStop", lazy.spawn("playerctl stop")),

   # Key([], "XF86AudioPlay", lazy.spawn("mpc toggle")),
   # Key([], "XF86AudioNext", lazy.spawn("mpc next")),
   # Key([], "XF86AudioPrev", lazy.spawn("mpc prev")),
   # Key([], "XF86AudioStop", lazy.spawn("mpc stop")),

# QTILE LAYOUT KEYS
    # Key([mod], "n", lazy.layout.normalize()),
    # Key([mod], "space", lazy.next_layout()),

# CHANGE FOCUS
    # Key([mod], "Up", lazy.layout.up()),
    # Key([mod], "Down", lazy.layout.down()),
    # Key([mod], "Left", lazy.layout.left()),
    # Key([mod], "Right", lazy.layout.right()),
    # Key([mod], "k", lazy.layout.up()),
    # Key([mod], "j", lazy.layout.down()),
    # Key([mod], "h", lazy.layout.left()),
    # Key([mod], "l", lazy.layout.right()),


# RESIZE UP, DOWN, LEFT, RIGHT
    # Key([mod, "control"], "l",
    #     lazy.layout.grow_right(),
    #     lazy.layout.grow(),
    #     lazy.layout.increase_ratio(),
    #     lazy.layout.delete(),
    #     ),
    # Key([mod, "control"], "Right",
    #     lazy.layout.grow_right(),
    #     lazy.layout.grow(),
    #     lazy.layout.increase_ratio(),
    #     lazy.layout.delete(),
    #     ),
    # Key([mod, "control"], "h",
    #     lazy.layout.grow_left(),
    #     lazy.layout.shrink(),
    #     lazy.layout.decrease_ratio(),
    #     lazy.layout.add(),
    #     ),
    # Key([mod, "control"], "Left",
    #     lazy.layout.grow_left(),
    #     lazy.layout.shrink(),
    #     lazy.layout.decrease_ratio(),
    #     lazy.layout.add(),
    #     ),
    # Key([mod, "control"], "k",
    #     lazy.layout.grow_up(),
    #     lazy.layout.grow(),
    #     lazy.layout.decrease_nmaster(),
    #     ),
    # Key([mod, "control"], "Up",
    #     lazy.layout.grow_up(),
    #     lazy.layout.grow(),
    #     lazy.layout.decrease_nmaster(),
    #     ),
    # Key([mod, "control"], "j",
    #     lazy.layout.grow_down(),
    #     lazy.layout.shrink(),
    #     lazy.layout.increase_nmaster(),
    #     ),
    # Key([mod, "control"], "Down",
    #     lazy.layout.grow_down(),
    #     lazy.layout.shrink(),
    #     lazy.layout.increase_nmaster(),
    #     ),


# FLIP LAYOUT FOR MONADTALL/MONADWIDE
    # Key([mod, "shift"], "f", lazy.layout.flip()),

# FLIP LAYOUT FOR BSP
    # Key([mod, "mod1"], "k", lazy.layout.flip_up()),
    # Key([mod, "mod1"], "j", lazy.layout.flip_down()),
    # Key([mod, "mod1"], "l", lazy.layout.flip_right()),
    # Key([mod, "mod1"], "h", lazy.layout.flip_left()),

# MOVE WINDOWS UP OR DOWN BSP LAYOUT
    # Key([mod, "shift"], "k", lazy.layout.shuffle_up()),
    # Key([mod, "shift"], "j", lazy.layout.shuffle_down()),
    # Key([mod, "shift"], "h", lazy.layout.shuffle_left()),
    # Key([mod, "shift"], "l", lazy.layout.shuffle_right()),

# MOVE WINDOWS UP OR DOWN MONADTALL/MONADWIDE LAYOUT
    # Key([mod, "shift"], "Up", lazy.layout.shuffle_up()),
    # Key([mod, "shift"], "Down", lazy.layout.shuffle_down()),
    # Key([mod, "shift"], "Left", lazy.layout.swap_left()),
    # Key([mod, "shift"], "Right", lazy.layout.swap_right()),

# TOGGLE FLOATING LAYOUT
    # Key([mod, "shift"], "space", lazy.window.toggle_floating()),

] ### keys END

groups = []

# FOR QWERTY KEYBOARDS
group_names = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0",]

# FOR AZERTY KEYBOARDS
# group_names = ["ampersand", "eacute", "quotedbl", "apostrophe", "parenleft", "section", "egrave", "exclam", "ccedilla", "agrave",]

# group_labels = ["1 ", "2 ", "3 ", "4 ", "5 ", "6 ", "7 ", "8 ", "9 ", "0", ]
# group_labels = ["", "", "", "", "", "", "", "", "", "", ]
group_labels = ["", "", "⎇", "", "", "", "", "", "♾", "☯", ]
# group_labels = ["Web", "Edit/chat", "Image", "Gimp", "Meld", "Video", "Vb", "Files", "Mail", "Music",]

group_layouts = ["treetab", "treetab", "treetab", "monadthreecol", "monadwide", "monadtall", "matrix", "monadtall", "monadtall", "monadthreecol"]
# group_layouts = ["monadtall", "matrix", "monadtall", "bsp", "monadtall", "matrix", "monadtall", "bsp", "monadtall", "monadtall",]

for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
        ))

for g in groups:
    keys.extend([

        # CHANGE WORKSPACES
        Key(WINKEY, g.name, lazy.group[g.name].toscreen()),

        # MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND STAY ON WORKSPACE
        Key(WIN_SHIFT, g.name, lazy.window.togroup(g.name)),
        # MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND FOLLOW MOVED WINDOW TO WORKSPACE
        # Key([mod, "shift"], i.name, lazy.window.togroup(i.name) , lazy.group[i.name].toscreen()),
    ])


# groups.append(
#     # ScratchPad("scratchpad", [
#     #     # define drop down group(s).
#         DropDown("term", myTermPlusShell, on_focus_lost_hide=False, opacity=0.95, height=0.45, width=0.66),
#     #     # DropDown("player", 'deadbeef', on_focus_lost_hide=False, opacity=0.95, height=0.55, width=0.66, y=0.1),
#     # ])
# )

# keys.extend([
#     # Scratchpad
#     # toggle visibiliy of above defined DropDown named "term"
#     Key([], 'F12', lazy.group['scratchpad'].dropdown_toggle('term')),
#     # Key(WIN_ALT, "p", lazy.group['scratchpad'].dropdown_toggle('player')),
# ])


def init_layout_theme():
    return dict(
        margin=5,
        border_width=2,
        border_focus=bar_colors['odd-text'][0],
        border_normal=bar_colors['even-icon-color'][0]
    )


layout_theme = init_layout_theme()


layouts = [
    layout.MonadTall(**layout_theme),
    layout.TreeTab(**layout_theme),
    # layout.Max(**layout_theme),
    layout.Floating(**layout_theme),
    layout.Matrix(**layout_theme),
    layout.MonadThreeCol(**layout_theme),
    layout.MonadWide(**layout_theme),
    # layout.Slice(**layout_theme),
    # layout.Tile(shift_windows=True, **layout_theme),
    # layout.Stack(num_stacks=2, **layout_theme),
    # layout.Bsp(**layout_theme),
    # layout.RatioTile(**layout_theme),
    # layout.Zoomy(**layout_theme)
]


# WIDGETS FOR THE BAR
def init_widgets_defaults():
    return dict(
        font="Noto Sans",
        fontsize=12,
        padding=2,
        background=bar_colors['bar-background']
    )


widget_defaults = init_widgets_defaults()


def init_widgets_list():
    _widgets_list = [
        # Left Half
        widget.Sep(
            linewidth=0,
            padding=7,
            background=bar_colors['odd-color']
        ),
        widget.Image(
            filename="~/.config/qtile/icons/python.png",
            mouse_callbacks={'Button1': lambda: QTILE.cmd_spawn('rofi -show run')},
            background=bar_colors['odd-color']
        ),
        arrow_widget(left=False, odd=False),
        widget.GroupBox(
            font="FontAwesome",
            fontsize=16,
            margin_y=3,
            margin_x=0,
            padding_y=6,
            padding_x=5,
            borderwidth=0,
            active=bar_colors['active-group'],
            inactive=bar_colors['inactive-group'],
            disable_drag=True,
            rounded=False,
            # highlight_color=dt_colors[1],
            highlight_method="text",
            this_current_screen_border=bar_colors['selected-group'],
            this_screen_border=bar_colors['odd-color'],
            other_current_screen_border=bar_colors['bar-background'],
            other_screen_border=bar_colors['bar-background'],
            foreground=bar_colors['even-text'],
            background=bar_colors['even-color']
        ),
        arrow_widget(left=False, odd=True),
        widget.CurrentLayoutIcon(
            custom_icon_paths=[os.path.expanduser("~/.config/qtile/icons")],
            padding=0,
            scale=0.7,
            foreground=bar_colors['odd-text'],
            background=bar_colors['odd-color']
        ),
        widget.CurrentLayout(
            font="Noto Sans Bold",
            foreground=bar_colors['odd-text'],
            background=bar_colors['odd-color']
        ),
        arrow_widget(left=False, background=bar_colors['bar-background']),
        widget.Sep(
            linewidth=0,
            padding=8,
            # foreground=bar_colors['odd-text'],
            # background=bar_colors['odd-color']
        ),
        # Window Name (center)
        widget.WindowName(
            font="Noto Sans Bold",
            # fontsize=12,
            foreground=bar_colors['bar-text'],
            # background=bar_colors['bar-background']
        ),
        # Right Half
        arrow_widget(left=True, background=bar_colors['bar-background']),
        widget.Net(
            font="Noto Sans Bold",
            # fontsize=12,
            interface="enp40s0",
            format='{down} ↓↑ {up}',
            padding=5,
            foreground=bar_colors['odd-text'],
            background=bar_colors['odd-color']
        ),
        widget.Sep(
            linewidth=1,
            padding=10,
            foreground=bar_colors['odd-icon-color'],
            background=bar_colors['odd-color']
        ),
        widget.NetGraph(
            font="Noto Sans Bold",
            # fontsize=12,
            bandwidth="down",
            interface="auto",
            type='box',
            foreground=bar_colors['odd-text'],
            background=bar_colors['odd-color'],
            fill_color=bar_colors['graph-color'],
            graph_color=bar_colors['graph-color'],
            border_color=bar_colors['odd-text'],
            padding=0,
            border_width=1,
            line_width=1,
        ),
        arrow_widget(left=True, odd=True),
        widget.TextBox(
            font="FontAwesome",
            text="  ",
            fontsize=16,
            padding=2,
            foreground=bar_colors['even-icon-color'],
            background=bar_colors['even-color']
        ),
        widget.ThermalSensor(
            font="Noto Sans Bold",
            foreground_alert=nord_colors['aurora-1'],
            foreground=bar_colors['even-text'],
            background=bar_colors['even-color'],
            metric=True,
            padding=3,
            threshold=70
        ),
        arrow_widget(left=True, odd=False),
        widget.TextBox(
            font="FontAwesome",
            text="  ",
            foreground=bar_colors['odd-icon-color'],
            background=bar_colors['odd-color'],
            padding=0,
            fontsize=16
        ),
        widget.CPUGraph(
            # border_color=colors[2],
            # fill_color=colors[8],
            # graph_color=colors[8],
            # background=bar_colors['odd-color'],
            foreground=bar_colors['odd-text'],
            background=bar_colors['odd-color'],
            fill_color=bar_colors['graph-color'],
            graph_color=bar_colors['graph-color'],
            border_color=bar_colors['odd-text'],
            border_width=1,
            line_width=1,
            core="all",
            type="box"
        ),
        arrow_widget(left=True, odd=True),
        widget.TextBox(
            font="FontAwesome",
            text="  ",
            foreground=bar_colors['even-icon-color'],
            background=bar_colors['even-color'],
            padding=0,
            fontsize=16
        ),
        widget.Memory(
            font="Noto Sans Bold",
            format='{MemUsed: .0f} M/{MemTotal: .0f} M ',
            update_interval=2,
            mouse_callbacks={'Button1': lambda: QTILE.cmd_spawn(myTermRun + 'htop')},
            # fontsize=12,
            foreground=bar_colors['even-text'],
            background=bar_colors['even-color'],
        ),
        arrow_widget(left=True, odd=False),
        widget.TextBox(
            mouse_callbacks = {'Button1': lambda: QTILE.cmd_spawn(myTermRun + 'khal interactive')},
            font="FontAwesome",
            text="  ",
            foreground=bar_colors['odd-icon-color'],
            background=bar_colors['odd-color'],
            padding=0,
            fontsize=16
        ),
        widget.Clock(
            font="Noto Sans Bold",
            foreground=bar_colors['odd-text'],
            background=bar_colors['odd-color'],
            format="%Y-%m-%d %a"
            # format="%A, %b %d  [ %H:%M:%S ]"
        ),
        widget.TextBox(
            font="FontAwesome",
            text="  ",
            foreground=bar_colors['odd-icon-color'],
            background=bar_colors['odd-color'],
            padding=0,
            fontsize=16
        ),
        widget.Clock(
            font="Noto Sans Bold",
            mouse_callbacks = {'Button1': lambda: QTILE.cmd_spawn(myTermRun + 'peaclock')},
            foreground=bar_colors['odd-text'],
            background=bar_colors['odd-color'],
            format="%H:%M:%S"
            # format="%A, %b %d  [ %H:%M:%S ]"
        ),
        arrow_widget(left=True, odd=True)
    ]
    return _widgets_list


def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()

    widgets_screen1.extend([
        # widget.TextBox(
        #     font="FontAwesome",
        #     text="  ",
        #     foreground=bar_colors['odd-icon-color'],
        #     background=bar_colors['odd-color'],
        #     padding=0,
        #     fontsize=16
        # ),
        # widget.Clock(
        #     foreground=bar_colors['odd-text'],
        #     background=bar_colors['odd-color'],
        #     format="%Y-%m-%d %a %H:%M:%S"
        #     # format="%A, %b %d  [ %H:%M:%S ]"
        # ),
        # arrow_widget(left=True, odd=True),
        # widget.Systray(
        #     background=bar_colors['even-color'],
        #     icon_size=20,
        #     padding=4
        # ),
        widget.TextBox(
            font="Noto Sans Bold",
            text="Vol: ",
            foreground=bar_colors['even-text'],
            background=bar_colors['even-color']
        ),
        widget.Volume(
            font="Noto Sans Bold",
            foreground=bar_colors['even-text'],
            background=bar_colors['even-color'],
            # icon_size=20,
            padding=5
        ),
        widget.Sep(
            linewidth=0,
            padding=7,
            background=bar_colors['even-color']
        )
    ])

    return widgets_screen1


def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()

    widgets_screen2.extend([
        # widget.Mpris2(
        #     foreground=bar_colors['odd-text'],
        #     background=bar_colors['odd-color'],
        #     name="deadbeef",
        #     objname="org.mpris.MediaPlayer2.DeaDBeeF"
        # ),
        # arrow_widget(left=True, odd=True),
        # widget.TextBox(
        #     font="Noto Sans Bold",
        #     text="Vol: ",
        #     foreground=bar_colors['even-text'],
        #     background=bar_colors['even-color']
        # ),
        # widget.Volume(
        #     font="Noto Sans Bold",
        #     foreground=bar_colors['even-text'],
        #     background=bar_colors['even-color'],
        #     # icon_size=20,
        #     padding=5
        # ),
        widget.Systray(
            background=bar_colors['even-color'],
            icon_size=20,
            padding=4
        ),
        widget.Sep(
            linewidth=0,
            padding=7,
            background=bar_colors['even-color']
        )
    ])

    return widgets_screen2


def init_widgets_screen3():
    widgets_screen3 = init_widgets_list()

    widgets_screen3.extend([
        widget.TextBox(
            font="FontAwesome",
            text="  ",
            foreground=bar_colors['even-icon-color'],
            background=bar_colors['even-color'],
            padding=0,
            fontsize=16
        ),
        widget.MemoryGraph(
            # border_color=colors[2],
            # fill_color=colors[8],
            # graph_color=colors[8],
            # background=bar_colors['odd-color'],
            foreground=bar_colors['even-text'],
            background=bar_colors['even-color'],
            fill_color=bar_colors['graph-color'],
            graph_color=bar_colors['graph-color'],
            border_color=bar_colors['even-text'],
            border_width=1,
            line_width=1,
            # core="all",
            type="box"
        ),
        widget.Sep(
            linewidth=0,
            padding=7,
            background=bar_colors['even-color']
        )
    ])

    return widgets_screen3


def init_widgets_screen4():
    return init_widgets_screen3()


def init_screens():
    return [
        Screen(top=bar.Bar(widgets=init_widgets_screen1(), size=20)),
        Screen(top=bar.Bar(widgets=init_widgets_screen3(), size=20)),
        Screen(top=bar.Bar(widgets=init_widgets_screen2(), size=20)),
        Screen(top=bar.Bar(widgets=init_widgets_screen4(), size=20))
    ]


if __name__ in ["config", "__main__"]:
    screens = init_screens()
    widgets_list = init_widgets_list()
    widgets_screen1 = init_widgets_screen1()
    widgets_screen2 = init_widgets_screen2()
    widgets_screen3 = init_widgets_screen3()
    widgets_screen4 = init_widgets_screen4()


# MOUSE CONFIGURATION
mouse = [
    Drag(WINKEY, "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag(WINKEY, "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click(WINKEY, "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []

# ASSIGN APPLICATIONS TO A SPECIFIC GROUPNAME
# BEGIN

# @hook.subscribe.client_new
# def assign_app_group(client):
#     d = {}
#     #########################################################
#     ################ assgin apps to groups ##################
#     #########################################################
#     d["1"] = ["Navigator", "Firefox", "Vivaldi-stable", "Vivaldi-snapshot", "Chromium", "Google-chrome", "Brave", "Brave-browser",
#               "navigator", "firefox", "vivaldi-stable", "vivaldi-snapshot", "chromium", "google-chrome", "brave", "brave-browser", ]
#     d["2"] = [ "Atom", "Subl3", "Geany", "Brackets", "Code-oss", "Code", "TelegramDesktop", "Discord",
#                "atom", "subl3", "geany", "brackets", "code-oss", "code", "telegramDesktop", "discord", ]
#     d["3"] = ["Inkscape", "Nomacs", "Ristretto", "Nitrogen", "Feh",
#               "inkscape", "nomacs", "ristretto", "nitrogen", "feh", ]
#     d["4"] = ["Gimp", "gimp" ]
#     d["5"] = ["Meld", "meld", "org.gnome.meld" "org.gnome.Meld" ]
#     d["6"] = ["Vlc","vlc", "Mpv", "mpv" ]
#     d["7"] = ["VirtualBox Manager", "VirtualBox Machine", "Vmplayer",
#               "virtualbox manager", "virtualbox machine", "vmplayer", ]
#     d["8"] = ["Thunar", "Nemo", "Caja", "Nautilus", "org.gnome.Nautilus", "Pcmanfm", "Pcmanfm-qt",
#               "thunar", "nemo", "caja", "nautilus", "org.gnome.nautilus", "pcmanfm", "pcmanfm-qt", ]
#     d["9"] = ["Evolution", "Geary", "Mail", "Thunderbird",
#               "evolution", "geary", "mail", "thunderbird" ]
#     d["0"] = ["Spotify", "Pragha", "Clementine", "Deadbeef", "Audacious",
#               "spotify", "pragha", "clementine", "deadbeef", "audacious" ]
#     ##########################################################
#     wm_class = client.window.get_wm_class()[0]
#
#     for i in range(len(d)):
#         if wm_class in list(d.values())[i]:
#             group = list(d.keys())[i]
#             client.togroup(group)
#             client.group.cmd_toscreen()

# END
# ASSIGN APPLICATIONS TO A SPECIFIC GROUPNAME


def get_window_assignments():
    d = dict()
    #########################################################
    # d["1"] = ["Navigator", "Firefox", "Vivaldi-stable", "Vivaldi-snapshot", "Chromium", "Google-chrome", "Brave", "Brave-browser",
    #           "navigator", "firefox", "vivaldi-stable", "vivaldi-snapshot", "chromium", "google-chrome", "brave", "brave-browser"]
    # d["1"] = ["Brave", "Brave-browser",
    #           "brave", "brave-browser"]
    d["2"] = ["Atom", "Subl3", "Geany", "Brackets", "Code-oss", "Code",
              "atom", "subl3", "geany", "brackets", "code-oss", "code"]
    d["4"] = ["Steam", "steam", "Lutris", "lutris", "Heroic", "heroic"]
    d["7"] = ["VirtualBox Manager", "VirtualBox Machine", "Vmplayer",
              "virtualbox manager", "virtualbox machine", "vmplayer", ]
    # d["8"] = ["emacs", "Emacs"]
    # d["9"] = ["BashTOP", "bashtop"]
    # d["0"] = ["Spotify", "Pragha", "Clementine", "Deadbeef", "Audacious", "Discord",
    #           "spotify", "pragha", "clementine", "deadbeef", "audacious", "discord"]
    d["0"] = ["Spotify", "Pragha", "Clementine", "Audacious", "Discord",
              "spotify", "pragha", "clementine", "audacious", "discord"]
    ##########################################################
    return d


window_assignment_dict = get_window_assignments()

floating_types = ["notification", "toolbar", "splash", "dialog"]


@hook.subscribe.client_new
def manage_new_windows(window: Window):

    # if window.window.get_wm_transient_for() or window.window.get_wm_type() in floating_types:
    #     window.floating = True
    #     return

    # add check for emacs capture frame
    if window.name == "doom-capture":
        window.floating = True
        return

    # doom everywhere module
    if "Emacs Everywhere" in window.name:
        window.floating = True
        return

    wm_class = window.window.get_wm_class()[0]

    for k, v in window_assignment_dict.items():
        if wm_class in v:

            window.togroup(k)

            current_group: Group = window.group
            current_screen: Screen = QTILE.current_screen

            if current_group != current_screen.group:
                # window.togroup(k)
                window.group.cmd_toscreen()


# Steam specific floating settings
@hook.subscribe.client_new
def float_steam(window):
    wm_class = window.window.get_wm_class()
    w_name = window.window.get_name()
    if (wm_class == ("Steam", "Steam") and (
        w_name not in ("Steam", "Friends List")
        # w_name == "Friends List"
        # or w_name == "Screenshot Uploader"
        # or w_name.startswith("Steam - News")
        or "PMaxSize" in window.window.get_wm_normal_hints().get("flags", ())
    )):
        window.floating = True


@hook.subscribe.startup_once
def start_once():
    # home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/scripts/autostart.sh'])
    # subprocess.Popen([myTerm, '-e', 'bashtop', '-t', 'bashtop', '--class', 'bashtop'])
    # pass


@hook.subscribe.startup
def start_always():
    # Set the cursor to something sane in X
    subprocess.Popen(['xsetroot', '-cursor_name', 'left_ptr'])


@hook.subscribe.screen_change
def restart_on_randr(ev):
    lazy.cmd_restart()


main = None

follow_mouse_focus = True
bring_front_click = False
cursor_warp = False

floating_layout = layout.Floating(float_rules=[
    # {'wmclass': 'Arcolinux-welcome-app.py'},
    # {'wmclass': 'Arcolinux-tweak-tool.py'},
    # {'wmclass': 'confirm'},
    # {'wmclass': 'dialog'},
    # {'wmclass': 'download'},
    # {'wmclass': 'error'},
    # {'wmclass': 'file_progress'},
    # {'wmclass': 'notification'},
    # {'wmclass': 'splash'},
    # {'wmclass': 'toolbar'},
    # {'wmclass': 'confirmreset'},
    # {'wmclass': 'makebranch'},
    # {'wmclass': 'maketag'},
    *layout.Floating.default_float_rules,
    Match(wm_class='Arandr'),
    Match(wm_class='feh'),
    Match(wm_class='deadbeef', title='Track Properties'),
    Match(wm_class='deadbeef', title='Search'),
    Match(wm_class='pcmanfm', title='Confirm File Replacing'),
    # {'wmclass': 'keepassxc'},
    Match(wm_class='Galculator'),
    Match(wm_class='arcolinux-logout'),
    Match(title='branchdialog'),
    Match(title='Open File'),
    Match(title='Origin'),
    Match(wm_class='pinentry-gtk-2'),
    Match(wm_class='ssh-askpass'),
    Match(wm_class='tilda'),
],  fullscreen_border_width=0, border_width=2, 
    border_focus=bar_colors['odd-text'][0], border_normal=bar_colors['even-icon-color'][0])

auto_fullscreen = True

focus_on_window_activation = "smart"  # or focus

wmname = "LG3D"
