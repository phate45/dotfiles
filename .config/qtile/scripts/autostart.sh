#!/bin/bash

function run {
  # if ! pgrep $1 ;
  if ! pgrep -x $(basename $1 | head -c 15) 1>/dev/null;
  then
    $@&
  fi
}

# Set your native resolution IF it does not exist in xrandr
# More info in the script
#run $HOME/.config/qtile/scripts/set-screen-resolution-in-virtualbox.sh

# Find out your monitor name with xrandr or arandr (save and you get this line)
#xrandr --output VGA-1 --primary --mode 1360x768 --pos 0x0 --rotate normal
#xrandr --output DP2 --primary --mode 1920x1080 --rate 60.00 --output LVDS1 --off &
#xrandr --output LVDS1 --mode 1366x768 --output DP3 --mode 1920x1080 --right-of LVDS1
#xrandr --output HDMI2 --mode 1920x1080 --pos 1920x0 --rotate normal --output HDMI1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output VIRTUAL1 --off
# xrandr --output HDMI-A-1 --pos 860x0 --output DisplayPort-0 --pos 0x1200 --output DisplayPort-1 --pos 1920x1200
# xrandr --refresh 144 --output DisplayPort-0 --pos 1920x1200 --output DisplayPort-1 --pos 0x1714 --output HDMI-A-0 --pos 0x514 --output HDMI-A-1 --pos 2614x0

# change your keyboard if you need it
#setxkbmap -layout be

# autostart ArcoLinux Welcome App
#run dex $HOME/.config/autostart/arcolinux-welcome-app.desktop &

# Some ways to set your wallpaper besides variety or nitrogen
#feh --bg-fill /usr/share/backgrounds/arcolinux/arco-wallpaper.jpg &
#start the conky to learn the shortcuts
# (conky -c $HOME/.config/qtile/scripts/system-overview) &

#IN BETA PHASE
#start sxhkd to replace Qtile native key-bindings
#run sxhkd -c ~/.config/qtile/sxhkd/sxhkdrc &

### END OF EXAMPLES

## RESOLUTION

# before latest shuffle (display port died on the bottom left dell)
# xrandr --output "DisplayPort-0" --mode 3440x1440 --pos 1920x1200 --refresh 144 --output "DisplayPort-1" --mode 1920x1200 --pos 0x1714 --output "HDMI-A-0" --pos 0x514 --output "HDMI-A-1" --pos 2614x0 --mode 1920x1200

# after shuffle
xrandr --output "DisplayPort-0" --primary --mode 3440x1440 --pos 1920x1200 --refresh 144 --rotate normal \
       --output "DisplayPort-1" --mode 1920x1200 --pos 0x240 --rotate normal \
       --output "HDMI-A-0" --mode 1920x1200 --pos 0x1440 --rotate normal \
       --output "HDMI-A-1" --mode 1920x1200 --pos 2414x0 --rotate normal

## APPS

# run variety &
run nm-applet &
run pamac-tray &
run volumeicon &
numlockx on &
# redshift-gtk &
# run xfce4-power-manager &
# blueberry-tray &

# picom --config $HOME/.config/qtile/scripts/picom.conf &
# doesn't quite work with high refresh rate new main screen

/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
/usr/lib/xfce4/notifyd/xfce4-notifyd &
# xautolock -time 5 -locker "i3lock -c 000000" &

# starting user applications at boot time

run syncthingtray-qt6 &
run keepassxc &
# nitrogen --restore &
# screenkey --start-disabled &
run virtualbox &
run virt-manager &
run emacs &
# run firefox &
# run chromium &
run flameshot &
run tilda &

sleep 1s
# run mattermost-desktop &
run discord &

sleep 2s
run subl &
run steam &
# run lutris &
run heroic &

# wallpaper setting
# fd . '/mnt/Data/Wallpapers' -t f -d 1 -a -0 | shuf -n 1 -z | xargs -0 -I W feh --bg-fill "W" &
feh --randomize --bg-fill '/mnt/Data/Wallpapers/'

# urxvtd -q -o -f &
#run caffeine -a &
#run vivaldi-stable &
#run thunar &
#run dropbox &
#run insync start &
#run spotify &
#run atom &
#run telegram-desktop &

sleep 3s

qtile cmd-obj -o cmd -f restart 
