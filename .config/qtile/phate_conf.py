from libqtile.lazy import lazy
from libqtile import widget

### MISC FUNCTIONS ###


# Brings all floating windows to the front
@lazy.function
def float_to_front(qtile):
    for group in qtile.groups:
        for window in group.windows:
            if window.floating:
                window.cmd_bring_to_front()


@lazy.function
def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        _index = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[_index - 1].name)


@lazy.function
def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        _index = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[_index + 1].name)


### COLORS ###

def init_colors():
    return [["#2F343F", "#2F343F"], # color 0
            ["#2F343F", "#2F343F"], # color 1
            ["#c0c5ce", "#c0c5ce"], # color 2
            ["#fba922", "#fba922"], # color 3
            ["#3384d0", "#3384d0"], # color 4
            ["#f3f4f5", "#f3f4f5"], # color 5
            ["#cd1f3f", "#cd1f3f"], # color 6
            ["#62FF00", "#62FF00"], # color 7
            ["#6790eb", "#6790eb"], # color 8
            ["#a9a9a9", "#a9a9a9"]] # color 9


colors = init_colors()

dt_colors = [["#292d3e", "#292d3e"],  # 0 - panel background
             ["#434758", "#434758"],  # 1 - background for current screen tab
             ["#ffffff", "#ffffff"],  # 2 - font color for group names
             ["#ff5555", "#ff5555"],  # 3 - border line color for current tab
             ["#8d62a9", "#8d62a9"],  # 4 - border line color for other tab and odd widgets
             ["#668bd7", "#668bd7"],  # 5 - color for the even widgets
             ["#e1acff", "#e1acff"]]  # 6 - window name


nord_colors = {  # https://www.nordtheme.com/docs/colors-and-palettes
    'night-0': ["#2E3440", "#2E3440"],
    'night-1': ["#3B4252", "#3B4252"],
    'night-2': ["#434C5E", "#434C5E"],
    'night-3': ["#4C566A", "#4C566A"],
    'snow-0': ["#D8DEE9", "#D8DEE9"],
    'snow-1': ["#E5E9F0", "#E5E9F0"],
    'snow-2': ["#ECEFF4", "#ECEFF4"],
    'frost-0': ["#8FBCBB", "#8FBCBB"],
    'frost-1': ["#88C0D0", "#88C0D0"],
    'frost-2': ["#81A1C1", "#81A1C1"],
    'frost-3': ["#5E81AC", "#5E81AC"],
    'aurora-0': ["#BF616A", "#BF616A"],
    'aurora-1': ["#D08770", "#D08770"],
    'aurora-2': ["#EBCB8B", "#EBCB8B"],
    'aurora-3': ["#A3BE8C", "#A3BE8C"],
    'aurora-4': ["#B48EAD", "#B48EAD"]
}


bar_colors = {
    'bar-background': nord_colors['frost-2'],
    'bar-text': nord_colors['night-0'],
    'odd-color': nord_colors['night-2'],
    'odd-text': nord_colors['frost-1'],
    'odd-icon-color': nord_colors['aurora-2'],
    'even-color': nord_colors['night-0'],
    'even-text': nord_colors['frost-2'],
    'even-icon-color': nord_colors['frost-3'],
    'active-group': nord_colors['aurora-2'],
    'inactive-group': nord_colors['frost-2'],
    'selected-group': nord_colors['aurora-3'],
    'graph-color': nord_colors['aurora-2']
}

bar_const = {
    'left-arrow': "",
    'right-arrow': ""
}

def arrow_widget(left=False, odd=False, background=None, foreground=None):
    _background = background if background else (bar_colors['odd-color'] if odd else bar_colors['even-color'])
    _foreground = foreground if foreground else (bar_colors['even-color'] if odd else bar_colors['odd-color'])

    return widget.TextBox(
        text=bar_const['left-arrow'] if left else bar_const['right-arrow'],
        background=_background,
        foreground=_foreground,
        padding=0,
        fontsize=37
    )
