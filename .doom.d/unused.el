;; file containing snippets and TODOs that are currently not used in the main config


;; org-related

;; org-fancy-priorities settings
(use-package! org-fancy-priorities
  :hook
  (org-mode . org-fancy-priorities-mode)
  :config
  (setq org-fancy-priorities-list '("⚡" "⬆" "☯" "⬇" "៙")))

(use-package! org-pretty-table
  :after org
  :config
  (setq global-org-pretty-table-mode t))

(use-package! org-link-beautify
  :after org
  :config
    (setq org-link-beautify-mode 1))


;; keycast

;; from https://github.com/seagle0128/doom-modeline/issues/405
(use-package! keycast
  :hook (after-init . keycast-mode)
  :commands keycast-mode
  :config
  (define-minor-mode keycast-mode
    "Show current command and its key binding in the mode line."
    :global t
    (if keycast-mode
        (progn
          (add-hook 'pre-command-hook 'keycast--update t)
          (add-to-list 'global-mode-string '("" keycast-mode-line " ")))
      (remove-hook 'pre-command-hook 'keycast--update)
      (setq global-mode-string (remove '("" keycast-mode-line " ") global-mode-string))))

  (pushnew! keycast-substitute-alist '(evil-next-line nil nil)
                                     '(evil-previous-line nil nil)
                                     '(evil-forward-char nil nil)
                                     '(evil-backward-char nil nil)
                                     '(self-insert-command nil nil))

  (custom-set-faces!
    '(keycast-command :inherit doom-modeline-debug
                      :height 0.9)
    '(keycast-key :inherit custom-modified
                  :height 1.1
                  :weight bold)))

;; prism

;; prism for rainbow colors
;; taken from https://github.com/meedstrom/doom.d/blob/master/lisp/98-late.el
;; This needs some more experimentation .. it doesn't work correctly on lsp/tree-sitter comment faces, for example
(use-package! prism
  :after doom-themes
  :init
  (setq! prism-parens t)
  ;; (setq! prism-lightens `(5 10 15))
  ;; (setq! prism-desaturations '(-2.5 0 2.5))
  ;; (setq! prism-num-faces 16)
  :config
  (prism-set-colors
    :num 16
    ;; :colors '("dodgerblue" "medium sea green" "sandy brown")
    ;; :colors (list "red" "orange" "yellow" "green" "blue" "cyan" "violet" "magenta")
    ;; :colors '("#ffffff" "#ff62d4" "#3fdfd0" "#fba849" "#9f80ff" "#4fe42f" "#fe6060" "#4fafff" "#f0dd60")
    :colors (list "#8fbcbb" "#d8dee9" "#81a1c1" "#d08770" "#ebcb8b" "#a3be8c" "#b48ead" "#bf616a") ; nord theme colors
    ;; :desaturations '(-2.5 0 2.5)
    ;; :lightens `(5 10 15)
    :desaturations (cl-loop for i from 0 below 16
                            collect (* i 3))
    :lightens (cl-loop for i from 0 below 16
                       collect (* 3 i))
    :comments-fn
    (lambda (color)
      (prism-blend color
                   (face-attribute 'font-lock-comment-face :foreground) 0.25))
    :strings-fn
    (lambda (color)
      (prism-blend color "white" 0.5)))
  (setq prism-parens t)
  ;; Replace rainbow-delimiters (it's on a dozen hooks in Doom).
  (fset 'rainbow-delimiters-mode #'prism-mode))
