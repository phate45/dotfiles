;; -*- no-byte-compile: t; -*-
;;; $DOOMDIR/packages.el

;; To install a package with Doom you must declare them here and run 'doom sync'
;; on the command line, then restart Emacs for the changes to take effect -- or
;; use 'M-x doom/reload'.


;; To install SOME-PACKAGE from MELPA, ELPA or emacsmirror:
;(package! some-package)

;; To install a package directly from a remote git repo, you must specify a
;; `:recipe'. You'll find documentation on what `:recipe' accepts here:
;; https://github.com/raxod502/straight.el#the-recipe-format
;(package! another-package
;  :recipe (:host github :repo "username/repo"))

;; If the package you are trying to install does not contain a PACKAGENAME.el
;; file, or is located in a subdirectory of the repo, you'll need to specify
;; `:files' in the `:recipe':
;(package! this-package
;  :recipe (:host github :repo "username/repo"
;           :files ("some-file.el" "src/lisp/*.el")))

;; If you'd like to disable a package included with Doom, you can do so here
;; with the `:disable' property:
;(package! builtin-package :disable t)

;; You can override the recipe of a built in package without having to specify
;; all the properties for `:recipe'. These will inherit the rest of its recipe
;; from Doom or MELPA/ELPA/Emacsmirror:
;(package! builtin-package :recipe (:nonrecursive t))
;(package! builtin-package-2 :recipe (:repo "myfork/package"))

;; Specify a `:branch' to install a package from a particular branch or tag.
;; This is required for some packages whose default branch isn't 'master' (which
;; our package manager can't deal with; see raxod502/straight.el#279)
;(package! builtin-package :recipe (:branch "develop"))

;; Use `:pin' to specify a particular commit to install.
;(package! builtin-package :pin "1a2b3c4d5e")


;; Doom's packages are pinned to a specific commit and updated from release to
;; release. The `unpin!' macro allows you to unpin single packages...
;(unpin! pinned-package)
;; ...or multiple packages
;(unpin! pinned-package another-pinned-package)
;; ...Or *all* packages (NOT RECOMMENDED; will likely break things)
;(unpin! t)

; org packages
(package! org-super-agenda :pin "f4f528985397c833c870967884b013cf91a1da4a")

;; org-modern replaces both org-pretty-table and org-superstar
;;  the latter comes from the +pretty flag on the org module
(package! org-modern :pin "7d037569bc4a05f40262ea110c4cda05c69b5c52")
;; (package! org-pretty-table
;;   :recipe (:host github :repo "Fuco1/org-pretty-table") :pin "7bd68b420d3402826fea16ee5099d04aa9879b78")
;;(package! org-pretty-tags :pin "5c7521651b35ae9a7d3add4a66ae8cc176ae1c76")
(package! org-transclusion :recipe (:host github :repo "nobiot/org-transclusion")
  :pin "cf51df7b87e0d32ba13ac5380557e81d9845d81b")

(package! org-auto-tangle)
(package! org-reverse-datetree)
; (package! org-link-beautify)
(package! org-appear :recipe (:host github :repo "awth13/org-appear")
  :pin "eb9f9db40aa529fe4b977235d86494b115281d17")
(package! org-ol-tree :recipe (:host github :repo "Townk/org-ol-tree")
  :pin "207c748aa5fea8626be619e8c55bdb1c16118c25")
(package! org-autolist)

(package! org-roam-ui
  :recipe (:host github
           :repo "org-roam/org-roam-ui"
           :files ("*.el" "out"))
  :pin "5ac74960231db0bf7783c2ba7a19a60f582e91ab")
(package! websocket :pin "82b370602fa0158670b1c6c769f223159affce9b") ; dependency of `org-roam-ui'
;; (package! consult-org-roam)  TODO https://github.com/jgru/consult-org-roam

;; (package! ob-restclient) TODO https://github.com/alf/ob-restclient.el
;; (package! ob-rust)  ;; not needed anymore? TODO verify
;; (package! ob-sql-mode) ;; TODO check out later - https://github.com/nikclayton/ob-sql-mode

; extra packages
(package! beacon)
(package! cargo-transient)
(package! comment-dwim-2)
(package! docker-compose-mode)
(package! elfeed-tube :recipe
  (:host github :repo "karthink/elfeed-tube"))
(package! elfeed-summary)

;; (package! golden-ratio) TODO https://github.com/roman/golden-ratio.el
; vertico enhancements
; inspired by: https://github.com/iamriel/doom.d
(package! hotfuzz :recipe
  (:host github
   :repo "axelf4/hotfuzz"
   :files ("hotfuzz.el")))

;; (package! keycast)
(package! page-break-lines :recipe (:host github :repo "purcell/page-break-lines")
  :pin "79eca86e0634ac68af862e15c8a236c37f446dcd")
(package! systemd :pin "b6ae63a236605b1c5e1069f7d3afe06ae32a7bae")
;; (package! prism)

; workaround
(package! closql :pin "0a7226331ff1f96142199915c0ac7940bac4afdd")

; unnecessary by default
(package! pipenv :disable t)
(package! org-gcal :disable t)
