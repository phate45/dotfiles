;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Marek Sekros"
      user-mail-address "marek.sekros@centrum.cz")

;; locale configuration
(doom-load-envvars-file (expand-file-name "myenv" doom-private-dir))
(setq system-time-locale "en_DK.UTF-8")

; (setq debug-on-error t)

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
; (setq doom-theme 'doom-one)
(setq doom-theme 'doom-nord)

;; font settings
(setq doom-font (font-spec :family "SauceCodePro Nerd Font Mono" :size 15)
      doom-big-font (font-spec :family "SauceCodePro Nerd Font Mono" :size 24)
      doom-variable-pitch-font (font-spec :family "Overpass Nerd Font" :size 15)
      doom-unicode-font (font-spec :family "Symbola")
      doom-serif-font (font-spec :family "IBM Plex Mono" :weight 'light)
      doom-symbol-fallback-font-families '("IBM Plex Sans" "Symbola" "Segoe UI Symbol"))

(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t)

  ;; paren config
  ;; turning off the box-style highlight in favor of simple bold underline
  (setq show-paren-style 'paren)
  (set-face-background 'show-paren-match (face-background 'default))
  (set-face-attribute 'show-paren-match nil :underline t :overline nil :box nil :weight 'extra-bold)
  ;; (set-face-attribute 'show-paren-match nil :box '(:line-width (-1 . -1) :color nil :style nil) :weight 'extra-bold)


  (custom-set-faces!
    '(font-lock-comment-face :slant italic)
    '(font-lock-keyword-face :slant italic)
    '(doom-modeline-buffer-modified :foreground "orange")
    '(quote :inherit org-quote))  ; define quote face by hand to avoid random font error
  )

;; theme-related stuff from tecosaur
(remove-hook 'window-setup-hook #'doom-init-theme-h)
(add-hook 'after-init-hook #'doom-init-theme-h 'append)
(delq! t custom-theme-load-path)

(after! doom-modeline
  (setq doom-modeline-major-mode-icon t
        doom-modeline-persp-name t))

(setq doom-fallback-buffer-name "► Doom"
      +doom-dashboard-name "► Doom")

;; ## Org mode settings ##
(setq org-directory "~/Sync/org")

;; misc org settings loaded after org itself
(after! org
  ; custom faces
  ; inspired by faces defined in doom's org module's config.el
  (with-no-warnings
    (custom-declare-face '+org-todo-custom  '((t (:inherit +org-todo-project :foreground "steel blue"))) "")
    (custom-declare-face '+org-onhold-custom  '((t (:inherit org-todo :foreground "sea green"))) "")
    (custom-declare-face '+org-done-custom  '((t (:inherit (italic org-done)))) ""))

  ; see https://github.com/jparcill/emacs_config/blob/master/config.el
  (use-package! calfw-org
    :after calfw)
  (map! :leader
       (:prefix-map ("n" . "notes")
        :desc "Org Agenda (calendar)" "A" #'cfw:open-org-calendar))

  (defadvice org-capture
    (after make-full-window-frame activate) "Advise capture to be the only window when used as a popup"
    (if (equal "doom-capture" (frame-parameter nil 'name))
      (delete-other-windows)))

  (defadvice org-capture-finalize
    (after delete-capture-frame activate) "Advise capture-finalize to close the frame"
    (if (equal "doom-capture" (frame-parameter nil 'name))
      (delete-frame)))

  (use-package! org-appear
    :hook (org-mode . org-appear-mode)
    :config
    (setq org-appear-autoemphasis t
          org-appear-autosubmarkers t
          org-appear-autolinks nil)
    ;; for proper first-time setup, `org-appear--set-elements'
    ;; needs to be run after other hooks have acted.
    (run-at-time nil nil #'org-appear--set-elements))

  (use-package! org-ol-tree
    :commands org-ol-tree
    :config
    ;; (defadvice! org-ol-tree-system--graphical-frame-p--pgtk ()
    ;;   :override #'org-ol-tree-system--graphical-frame-p
    ;;   (memq window-system '(pgtk x w32 ns))))
    ; borrowed from tecosaur's config
    (setq org-ol-tree-ui-icon-set
          (if (and (display-graphic-p)
                   (fboundp 'nerd-icons-material))
              'nerd-icons
            'unicode))
    (org-ol-tree-ui--update-icon-set))

  (map! :map org-mode-map
        :localleader
        :desc "Outline" "O" #'org-ol-tree)

  (use-package! org-autolist
    :config
    (add-hook 'org-mode-hook #'org-autolist-mode))

  ;; from http://mbork.pl/2021-05-02_Org-mode_to_Markdown_via_the_clipboard
  (defun org-copy-region-as-markdown ()
    "Copy the region (in Org) to the system clipboard as Markdown."
    (interactive)
    (if (use-region-p)
        (let* ((region
                (buffer-substring-no-properties
                        (region-beginning)
                        (region-end)))
               (markdown
                (org-export-string-as region 'md t '(:with-toc nil))))
          (gui-set-selection 'CLIPBOARD markdown))))

  ;; more agenda enhancements? -- Nan0Scho1ar on doom discord#emacs-lisp
  ;; (defun find-org-files-in-dir (dir &optional base-dir)
  ;;   "Find all org files in each of the child dirs in BASE-DIR."
  ;;     (directory-files-recursively (expand-file-name dir base-dir) "\.org$"))

  ;; (defun find-org-files-in-dirs (base-dir children)
  ;;   "Find all org files in each of the child dirs in BASE-DIR."
  ;;   (mapcan (lambda (x) (find-org-files-in-dir x base-dir)) children))

  ;; (setq org-directory "~/nextcloud/org/"
  ;;       n0s1-agenda-dirs '("personal" "projects" "todos" "work")
  ;;       org-agenda-files (find-org-files-in-dirs org-directory n0s1-agenda-dirs)))

  ;; from https://emacs.stackexchange.com/a/60555
  (defun farynaio/org-link-copy (&optional arg)
  "Extract URL from org-mode link and add it to kill ring."
  (interactive "P")
  (let* ((link (org-element-lineage (org-element-context) '(link) t))
          (type (org-element-property :type link))
          (url (org-element-property :path link))
          (url (concat type ":" url)))
    (kill-new url)
    (message (concat "Copied URL: " url))))

  (define-key org-mode-map (kbd "C-x C-l") 'farynaio/org-link-copy)

  ;; from https://www.tquelch.com/posts/emacs-config/
  (defun tq/verify-refile-target ()
    "Exclude done todo states from refile targets"
    (not (member (nth 2 (org-heading-components)) org-done-keywords)))

  (map! :map org-agenda-mode-map
      [remap org-agenda-next-line] #'org-agenda-next-item
      [remap org-agenda-previous-line] #'org-agenda-previous-item)

  (advice-add 'org-agenda-todo :after #'org-save-all-org-buffers)

  (setq org-ellipsis " ▼ "
        org-hide-leading-stars t
        org-hide-emphasis-markers t
        org-use-property-inheritance t
        org-list-allow-alphabetical t
        org-export-in-background t
        org-fold-catch-invisible-edits 'smart
        org-pretty-entities t
        org-startup-folded  'content
        org-list-demote-modify-bullet '(("+" . "-") ("-" . "+") ("*" . "+") ("1." . "a."))
        org-fontify-quote-and-verse-blocks t
        org-inline-src-prettify-results '("⟨" . "⟩")
        org-auto-align-tags nil
        org-tags-column 0
        org-treat-insert-todo-heading-as-state-change t
        org-treat-S-cursor-todo-selection-as-state-change nil
        org-insert-heading-respect-content t
        doom-themes-org-fontify-special-tags nil

        ; agenda settings
        org-agenda-files (directory-files-recursively org-directory "\.org$")
        org-agenda-skip-unavailable-files t
        org-agenda-skip-deadline-if-done t
        org-agenda-skip-scheduled-if-done t
        org-agenda-skip-timestamp-if-done t
        org-agenda-todo-ignore-with-date t
        org-agenda-tags-column 0
        org-agenda-sticky t
        org-agenda-time-grid '((daily today require-timed)
                               (800 1000 1200 1400 1600 1800 2000)
                               " ┄┄┄┄┄ " "┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄")
        org-agenda-current-time-string "⭠ now ─────────────────────────────────────────────────"
        org-agenda-breadcrumbs-separator " ❱ "

        org-log-into-drawer t
        org-log-done        'time
        org-log-reschedule  'time
        org-log-refile      'time
        org-log-redeadline  'time

        ; priorities
        ; expanded from the default A-C to match the 5 fancy priorities symbols defined below
        org-priority-highest ?A
        org-priority-default ?C
        org-priority-lowest  ?E
        org-priority-faces '((?A . 'nerd-icons-red)
                             (?B . 'nerd-icons-orange)
                             (?C . 'nerd-icons-yellow)
                             (?D . 'nerd-icons-green)
                             (?E . 'nerd-icons-blue))

        org-todo-keyword-faces
        '(("STARTED" . +org-todo-active)
          ("WAIT" . +org-todo-onhold)
          ("HOLD" . +org-todo-onhold)
          ("IDEA" . +org-todo-custom)
          ("PROJECT" . +org-todo-project)
          ("MAYBE" . +org-onhold-custom)
          ("KILL" . +org-done-custom))

        ; org-refile settings
        org-refile-targets
          '((nil :maxlevel . 3)
            (org-agenda-files :maxlevel . 3))

        org-refile-use-outline-path 'file
        org-refile-path-complete-in-steps nil
        org-refile-allow-creating-parent-nodes 'confirm
        org-refile-target-verify-function 'tq/verify-refile-target
        org-todo-keywords
          '((sequence
             "TODO(t)"          ; A task that needs doing & is ready to do
             "STARTED(s!)"      ; A task that is in progress
             "IDEA(i@)"         ; An idea, to be elaborated on
             "PROJECT(p@)"      ; A project, which usually contains other tasks
             "WAIT(w@/!)"         ; Something external is holding up this task
             "HOLD(h@/!)"         ; This task is paused/on hold because of me
             "MAYBE(m@/!)"        ; Consider what to do with this item
             "|"
             "DONE(d)"         ; Task successfully completed
             "KILL(k@)")        ; Task was cancelled, aborted or is no longer applicable
            (sequence
             "[ ](T)"   ; A task that needs doing
             "[-](S)"   ; Task is in progress
             "[?](W)"   ; Task is being held up or paused
             "|"
             "[X](D)") ; Task was completed
            (sequence
             "MEET(M)"
             "|"
             "CANC(C@)"
             "RESCH(R@)"))

        org-capture-templates (quote (
          ("u" "URL" entry
            (file+function "~/Sync/org/Inbox.org" org-reverse-datetree-goto-date-in-file)
"* [[%^{URL}][%^{Description}]] %^g
:properties:
:created_at: %U
:end:
%?
")

          ("n" "Notes" entry
           (file+function "~/Sync/org/Inbox.org" org-reverse-datetree-goto-date-in-file)
;;             "* %^{Description} %^g
"* %^{Description}
:properties:
:created_at: %U
:end:
%?
")

          ("t" "Task" entry
           (file+function "~/Sync/org/Inbox.org" org-reverse-datetree-goto-date-in-file)
"* TODO %^{Description} %^g
:properties:
:created_at: %U
:end:
%?
")
          ("l" "Log Time" entry
           (file+function "~/Sync/org/daily_notes.org" org-reverse-datetree-goto-date-in-file)
           "** %U - %^{Activity}"
           :immediate-finish t)
          )))

  (appendq! +ligatures-extra-symbols
          `(:list_property "∷"
            :em_dash       "—"
            :ellipses      "…"
            ; :title         "𝙏"
            ; :subtitle      "𝙩"
            ; :author        "𝘼"
            ; :date          "𝘿"
            ; :property      "𝒫"
            ; :setupfile     "₰"
            ; :startup       "𝓢"
            ; :options       "𝓞"
            ; :latex_class   "🄲"
            ; :latex_header  "⇥"
            ; :beamer_header "↠"
            ; :attr_latex    "🄛"
            ; :attr_html     "🄗"
            ; :begin_quote   "❮"
            ; :end_quote     "❯"
            ; :caption       "☰"
            ; :header        "›"
            ; :results       "🠶"
            ; :begin_export  "⯮"
            ; :end_export    "⯬"
            :arrow_right   "→"
            :arrow_left    "←"
            :arrow_lr      "⟷"
            :logbook       "Ŀ"
            :properties    "𝙋"
            :end           "𝘌"
            :refs          "𝓡"
            :id            "𝓘"
            :tags          "ナ"
            :key           "Ҡ"
            :alias         "@"
            :priority_a   ,(propertize "⚡" 'face 'nerd-icons-red)
            :priority_b   ,(propertize "⬆" 'face 'nerd-icons-orange)
            :priority_c   ,(propertize "☯" 'face 'nerd-icons-yellow)
            :priority_d   ,(propertize "⬇" 'face 'nerd-icons-green)
            :priority_e   ,(propertize "❓" 'face 'nerd-icons-blue)))

  (defadvice! +org-init-appearance-h--no-ligatures-a ()
    :after #'+org-init-appearance-h
    (set-ligatures! 'org-mode
      :name nil
      :src_block nil
      :src_block_end nil
      :quote nil
      :quote_end nil))

  (set-ligatures! 'org-mode
    :merge t
    :list_property "::"
    :em_dash       "---"
    :ellipsis      "..."
    :arrow_right   "->"
    :arrow_left    "<-"
    :arrow_lr      "<->"
    ; :title         "#+title:"
    ; :title         "#+TITLE:"
    ; :subtitle      "#+subtitle:"
    ; :author        "#+author:"
    ; :date          "#+date:"
    ; :date          ":date:"
    ; :property      "#+property:"
    ; :setupfile     "#+setupfile:"
    ; :setupfile     "#+SETUPFILE:"
    ; :startup       "#+startup:"
    ; :startup       "#+STARTUP:"
    ; :options       "#+options:"
    ; :latex_class   "#+latex_class:"
    ; :latex_header  "#+latex_header:"
    ; :beamer_header "#+beamer_header:"
    ; :attr_latex    "#+attr_latex:"
    ; :attr_html     "#+attr_html:"
    ; :begin_quote   "#+begin_quote"
    ; :end_quote     "#+end_quote"
    ; :caption       "#+caption:"
    ; :header        "#+header:"
    ; :begin_export  "#+begin_export"
    ; :end_export    "#+end_export"
    ; :results       "#+RESULTS:"
    ; :results       "#+results:"
    :logbook       ":LOGBOOK:"
    :logbook       ":logbook:"
    :properties    ":PROPERTIES:"
    :properties    ":properties:"
    :end           ":END:"
    :end           ":end:"
    :refs          ":ROAM_REFS:"
    :refs          ":roam_refs:"
    :id            ":ID:"
    :id            ":id:"
    :tags          ":tags:"
    :tags          "#+roam_tags:"
    :tags          "#+ROAM_TAGS:"
    ; :tags          "#+filetags:"
    ; :tags          "#+FILETAGS:"
    :key           "#+roam_key:"
    :key           "#+ROAM_KEY:"
    :alias         "#+roam_alias:"
    :alias         "#+ROAM_ALIAS:"
    :priority_a    "[#A]"
    :priority_b    "[#B]"
    :priority_c    "[#C]"
    :priority_d    "[#D]"
    :priority_e    "[#E]")
  ; (plist-put +ligatures-extra-symbols :name "⁍")

  ; source: tecosaur (https://github.com/tecosaur/emacs-config/commit/9c69d7bb728f209340254eb14c496494d8cb4294)
  (defadvice! +org-indent--reduced-text-prefixes ()
    :after #'org-indent--compute-prefixes
    (setq org-indent--text-line-prefixes
          (make-vector org-indent--deepest-level nil))
    (when (> org-indent-indentation-per-level 0)
      (dotimes (n org-indent--deepest-level)
        (aset org-indent--text-line-prefixes
              n
              (org-add-props
                  (concat (make-string (* n (1- org-indent-indentation-per-level))
                                       ?\s)
                          (if (> n 0)
                               (char-to-string org-indent-boundary-char)
                            "\u200b"))
                  nil 'face 'org-indent)))))

  (add-hook 'org-mode-hook #'+org-pretty-mode))


;; org-roam settings
(use-package! org-roam
  :after org
  :demand t
  :init
  (setq org-roam-directory "~/Sync/roam"
        org-roam-db-location "~/Sync/roam/org-roam.db"
        ;; +org-roam-auto-backlinks-buffer t

        org-roam-capture-templates
        '(("d" "default" plain (file "~/Sync/roam/default_template.org")
           :target (file+head "%<%Y%m%d%H%M%S>-${slug}.org"
                              "#+filetags:\n#+title: ${title}\n#+date: %<%Y-%m-%d>\n#+startup: showall\n")
           :unnarrowed t)

          ("b" "blog" plain (file "~/Sync/roam/blog_template.org")
           :target (file+head "blog/${slug}.org"
                              "#+filetags: :blog:\n#+title: ${title}\n#+date: %<%Y-%m-%d>\n#+startup: showall\n")
           :empty-lines-before 1
           :unnarrowed t)

          ("k" "bookmark" plain (file "~/Sync/roam/ref_template.org")
           :target (file+head "bookmarks/$%<%Y%m%d%H%M%S>-{slug}.org"
                             ":properties:\n:roam_refs: ${ref}\n:end:\n#+title: ${title}\n#+filetags: :bookmark:\n#+startup: showall\n\n")
           :empty-lines-before 1
           :unnarrowed t))

        org-roam-dailies-directory "dailies/"
        org-roam-dailies-capture-templates
        '(("d" "default" entry "* %?"
           :target (file+head "%<%Y-%m-%d>.org" "#+filetags: :daily:\n#+title: %<%Y-%m-%d>\n#+startup: showall\n\n")
           :empty-lines-before 1
           :unnarrowed 1))

        org-roam-capture-ref-templates
        '(("r" "ref" plain (file "~/Sync/roam/ref_template.org")
          :target (file+head "bookmarks/%<%Y%m%d%H%M%S>-${slug}.org"
                             ":properties:\n:roam_refs: ${ref}\n:end:\n#+title: ${title}\n#+filetags: :bookmark:\n#+startup: showall\n\n")
          :unnarrowed t)))

  (load! "roam-agenda.el")
  (add-to-list 'org-tags-exclude-from-inheritance "agenda")

  ;; written by Henrik himself (#help-doom-users discord channel)
  (defadvice! suppress-prompts-after-org-roam-capture-a (fn &rest args)
    :around #'org-roam-capture--finalize
    (letf! ((#'yes-or-no-p #'always)) (apply fn args)))

  ;; also from https://www.tquelch.com/posts/emacs-config/
  (add-hook! 'after-save-hook
             (defun org-rename-to-new-title ()
               (when-let*
                   ((old-file (buffer-file-name))
                    (is-roam-file (org-roam-file-p old-file))
                    (in-roam-base-directory? (string-equal
                                              (expand-file-name org-roam-directory)
                                              (file-name-directory old-file)))
                    (file-node (save-excursion
                                 (goto-char 1)
                                 (org-roam-node-at-point)))
                    (slug (org-roam-node-slug file-node))
                    (new-file (expand-file-name (concat slug ".org")))
                    (different-name? (not (string-equal old-file new-file))))
                 (rename-buffer new-file)
                 (rename-file old-file new-file)
                 (set-visited-file-name new-file)
                 (set-buffer-modified-p nil))))

  :config
  (map! :leader
        :prefix "n r"
        :desc "Add tag" "t" #'org-roam-tag-add
        :desc "Remove tag" "T" #'org-roam-tag-remove
        :desc "Add alias" "a" #'org-roam-alias-add
        :desc "Remove alias" "A" #'org-roam-alias-remove)

  (require 'org-roam-dailies)
  (require 'org-protocol)
  (require 'org-roam-protocol))

;; org-modern settings
(use-package! org-modern
  ;; :hook (org-mode . org-modern-mode)
  :config
  (setq org-modern-star '("◉" "○" "✸" "✿" "✤" "✜" "◆" "▶")
        org-modern-table-vertical 1
        org-modern-table-horizontal 0.2
        org-modern-list '((43 . "➤")
                          (45 . "–")
                          (42 . "•"))
        org-modern-todo-faces
        '(("TODO"     :inverse-video t :inherit org-todo)
          ("PROJECT"  :inverse-video t :inherit +org-todo-project)
          ("IDEA"     :inverse-video t :inherit +org-todo-project)
          ("STARTED"  :inverse-video t :inherit +org-todo-active)
          ("[ ]"      :inverse-video t :inherit org-todo)
          ("[-]"      :inverse-video t :inherit +org-todo-active)
          ("HOLD"     :inverse-video t :inherit +org-todo-onhold)
          ("WAIT"     :inverse-video t :inherit +org-todo-onhold)
          ("MAYBE"    :inverse-video t :inherit +org-todo-onhold)
          ("[?]"      :inverse-video t :inherit +org-todo-onhold)
          ("KILL"     :inverse-video t :inherit +org-todo-cancel)
          ("MEET"     :inverse-video t :inherit org-todo)
          ("CANC"     :inverse-video t :inherit +org-todo-cancel)
          ("RESCH"    :inverse-video t :inherit +org-todo-cancel)
          ("[X]"      :inverse-video t :inherit +org-todo-cancel)
          ("NO"       :inverse-video t :inherit +org-todo-cancel))
        org-modern-footnote
        (cons nil (cadr org-script-display))
        org-modern-block-fringe nil
        org-modern-block-name
        '((t . t)
          ("src" "»" "«")
          ("example" "»–" "–«")
          ("quote" "❝" "❞")
          ("export" "⏩" "⏪"))
        org-modern-progress nil
        org-modern-priority nil
        org-modern-horizontal-rule (make-string 36 ?─)
        org-modern-keyword
        '((t . t)
          ("title" . "𝙏")
          ("TITLE" . "𝙏")
          ("subtitle" . "𝙩")
          ("author" . "𝘼")
          ("email" . #("" 0 1 (display (raise -0.14))))
          ("date" . "𝘿")
          ("property" . "☸")
          ("options" . "⌥")
          ("startup" . "⏻")
          ("STARTUP" . "⏻")
          ("filetags" . "ナ")
          ("FILETAGS" . "ナ")
          ("macro" . "𝓜")
          ("bind" . #("" 0 1 (display (raise -0.1))))
          ("bibliography" . "")
          ("print_bibliography" . #("" 0 1 (display (raise -0.1))))
          ("cite_export" . "⮭")
          ("print_glossary" . #("ᴬᶻ" 0 1 (display (raise -0.1))))
          ("glossary_sources" . #("" 0 1 (display (raise -0.14))))
          ("import" . "⇤")
          ("setupfile" . "⇚")
          ("SETUPFILE" . "⇚")
          ("html_head" . "🅷")
          ("html" . "🅗")
          ("latex_class" . "🄻")
          ("latex_class_options" . #("🄻" 1 2 (display (raise -0.14))))
          ("latex_header" . "🅻")
          ("latex_header_extra" . "🅻⁺")
          ("latex" . "🅛")
          ("beamer_theme" . "🄱")
          ("beamer_color_theme" . #("🄱" 1 2 (display (raise -0.12))))
          ("beamer_font_theme" . "🄱𝐀")
          ("beamer_header" . "🅱")
          ("beamer" . "🅑")
          ("attr_latex" . "🄛")
          ("attr_html" . "🄗")
          ("attr_org" . "⒪")
          ("call" . #("" 0 1 (display (raise -0.15))))
          ("name" . "⁍")
          ("header" . "›")
          ("caption" . "☰")
          ("RESULTS" . "🠶")))
  (custom-set-faces! '(org-modern-statistics :inherit org-checkbox-statistics-todo))
  (global-org-modern-mode))

(after! spell-fu
  (cl-pushnew 'org-modern-tag (alist-get 'org-mode +spell-excluded-faces-alist)))

;; org-super-agenda settings
(use-package! org-super-agenda
  :after org-agenda
  :init
  ;; init groups
  (setq org-super-agenda-groups '((:name "Today"
                                   :time-grid t
                                   :scheduled today)
                                  (:name "Due today"
                                   :deadline today)
                                  (:name "Important"
                                   :priority "A")
                                  (:name "Overdue"
                                   :deadline past)
                                  (:name "Due soon"
                                   :deadline future)))
  :commands org-super-agenda-mode)
  ;; :config
  ;; (unless org-super-agenda-mode (org-super-agenda-mode)))

(after! org-agenda
  ;; (add-hook 'org-agenda-finalize-hook #'org-agenda-goto-today)
  (org-super-agenda-mode))

(use-package! org-roam-ui
  :after org-roam
  :commands org-roam-ui-open
  :hook (org-roam . org-roam-ui-mode)
  :config
  (require 'org-roam) ; in case autoloaded
  (defun org-roam-ui-open ()
    "Ensure the server is active, then open the roam graph."
    (interactive)
    (unless org-roam-ui-mode (org-roam-ui-mode 1))
    (browse-url-xdg-open (format "http://localhost:%d" org-roam-ui-port))))

; (use-package! org-link-beautify
;   :config (org-link-beautify-mode 1))

(use-package! org-transclusion
  :commands org-transclusion-mode
  :init
  (map! :after org :map org-mode-map
        "<f12>" #'org-transclusion-mode))

(use-package! org-auto-tangle
  :after org
  :defer t
  :hook (org-mode . org-auto-tangle-mode)
  :config (setq org-auto-tangle-default nil))
; enable auto tangling with "#+auto_tangle: t" in the header

;; end of org-specific configuration

(defvar +zen-serif-p t
  "Whether to use a serifed font with `mixed-pitch-mode'.")
(defvar +zen-org-starhide t
  "The value `org-modern-hide-stars' is set to.")

(setq +zen-text-scale 0.8)
(after! writeroom-mode
  ;; (defvar-local +zen--original-org-indent-mode-p nil)
  (defvar-local +zen--original-mixed-pitch-mode-p nil)

  (defun +zen-enable-mixed-pitch-mode-h ()
    "Enable `mixed-pitch-mode' when in `+zen-mixed-pitch-modes'."
    (when (apply #'derived-mode-p +zen-mixed-pitch-modes)
      (if writeroom-mode
          (progn
            (setq +zen--original-mixed-pitch-mode-p mixed-pitch-mode)
            (funcall (if +zen-serif-p #'mixed-pitch-serif-mode #'mixed-pitch-mode) 1))
        (funcall #'mixed-pitch-mode (if +zen--original-mixed-pitch-mode-p 1 -1)))))
  (defun +zen-prose-org-h ()
    "Reformat the current Org buffer appearance for prose."
    (when (eq major-mode 'org-mode)
      (setq display-line-numbers nil
            visual-fill-column-width 80
            ;; org-adapt-indentation nil
            )
      (when (featurep 'org-modern)
        (setq-local org-modern-star '("🙘" "🙙" "🙚" "🙛")
                    ;; org-modern-star '("🙐" "🙑" "🙒" "🙓" "🙔" "🙕" "🙖" "🙗")
                    org-modern-hide-stars +zen-org-starhide)
        (org-modern-mode -1)
        (org-modern-mode 1))
      ;; (setq
      ;;  +zen--original-org-indent-mode-p org-indent-mode)
      ;; (org-indent-mode -1)))
      ))
  (defun +zen-nonprose-org-h ()
    "Reverse the effect of `+zen-prose-org'."
    (when (eq major-mode 'org-mode)
      (when (bound-and-true-p org-modern-mode)
        (org-modern-mode -1)
        (org-modern-mode 1))
      ;; (when +zen--original-org-indent-mode-p (org-indent-mode 1))
      ))
  (pushnew! writeroom--local-variables
            'display-line-numbers
            'visual-fill-column-width
            ;; 'org-adapt-indentation
            'org-modern-mode
            'org-modern-star
            'org-modern-hide-stars)
  (add-hook 'writeroom-mode-enable-hook #'+zen-prose-org-h)
  (add-hook 'writeroom-mode-disable-hook #'+zen-nonprose-org-h))


(use-package! emacs-everywhere
  :if (daemonp)
  :config
  (require 'spell-fu)
  (setq emacs-everywhere-major-mode-function #'org-mode
        emacs-everywhere-frame-name-format "Edit ∷ %s — %s")
  (defadvice! emacs-everywhere-name-hack ()
    :after #'emacs-everywhere-set-frame-name
    (setq emacs-everywhere-frame-name
          (format emacs-everywhere-frame-name-format
                  (emacs-everywhere-app-class emacs-everywhere-current-app)
                  (truncate-string-to-width
                   (emacs-everywhere-app-title emacs-everywhere-current-app)
                   45 nil nil "…")))))

(use-package! evil
  :init
  (setq evil-respect-visual-line-mode t
        evil-kill-on-visual-paste nil))

(after! evil
  (setq evil-vsplit-window-right t
        evil-split-window-below t
        evil-want-fine-undo t)

  (defadvice! prompt-for-buffer (&rest _)
    :after '(evil-window-split evil-window-vsplit)
      (consult-buffer))

  (map! :map evil-window-map
    "SPC" #'evil-window-rotate-downwards
    ;; Navigation
    "<left>"     #'evil-window-left
    "<down>"     #'evil-window-down
    "<up>"       #'evil-window-up
    "<right>"    #'evil-window-right
    ;; Swapping windows
    "C-<left>"       #'+evil/window-move-left
    "C-<down>"       #'+evil/window-move-down
    "C-<up>"         #'+evil/window-move-up
    "C-<right>"      #'+evil/window-move-right)

  (map! :map evil-motion-state-map "g s l" #'avy-goto-line))  ;; duplicate here because the one below isn't applied correctly
;; todo investigate multicursor options
;;  search: "evil-mc-make-and-goto"
;;  > evil-normal-state-map
;;  > evil-visual-state-map
;;  ("C-c C->" . 'mc/mark-all-like-this-dwim)
;;  ("C-c C-'" . 'mc/mark-all-like-this-in-defun))
;;
;; (setq warning-suppress-types (quote ((undo discard-info)))) ?


(after! avy
  :config
  (setq avy-all-windows t
        avy-all-windows-alt nil)
  ;; from https://github.com/Zetagon/literate-dotfiles/blob/master/config.org
  (map! :map isearch-mode-map "M-j" #'avy-isearch)
  (map! :map evil-motion-state-map "g s l" #'avy-goto-line))

(after! which-key
  (setq which-key-idle-delay 0.5
        which-key-idle-secondary-delay 0.2
        which-key-allow-multiple-replacements t
        which-key-use-C-h-commands t
        which-key-show-early-on-C-h t
        prefix-help-command #'which-key-C-h-dispatch)

  (defadvice! fix-which-key-dispatcher-a (fn &rest args)
    :around #'which-key-C-h-dispatch
    (let ((keys (this-command-keys-vector)))
      (if (equal (elt keys (1- (length keys))) ?\?)
          (let ((keys (which-key--this-command-keys)))
            (embark-bindings (seq-take keys (1- (length keys)))))
        (apply fn args))))

  (pushnew!
   which-key-replacement-alist
   '(("" . "\\`+?evil[-:]?\\(?:a-\\)?\\(.*\\)") . (nil . "◂\\1"))
   '(("\\`g s" . "\\`evilem--?motion-\\(.*\\)") . (nil . "◃\\1"))
   ))

(setq projectile-ignored-projects '("~/" "/tmp" "~/.config/emacs/.local/straight/repos/"))
(defun projectile-ignored-project-function (filepath)
  "Return t if FILEPATH is within any of `projectile-ignored-projects'"
  (or (mapcar (lambda (p) (s-starts-with-p p filepath)) projectile-ignored-projects)))


(after! emojify
  (defvar emojify-disabled-emojis
    '(;; Org
      "◼" "☑" "☸" "⚙" "⏩" "⏪" "⬆" "⬇" "❓" "☯" "⚡"
      ;; Terminal powerline
      "✔"
      ;; Box drawing
      "▶" "◀"
      ;; I just want to see this as text
      "©" "™")
    "Characters that should never be affected by `emojify-mode'.")

  (defadvice! emojify-delete-from-data ()
    "Ensure `emojify-disabled-emojis' don't appear in `emojify-emojis'."
    :after #'emojify-set-emoji-data
    (dolist (emoji emojify-disabled-emojis)
      (remhash emoji emojify-emojis))))

(after! marginalia
  (setq marginalia-censor-variables nil)

  (defadvice! +marginalia--anotate-local-file-colorful (cand)
    "Just a more colourful version of `marginalia--anotate-local-file'."
    :override #'marginalia--annotate-local-file
    (when-let (attrs (file-attributes (substitute-in-file-name
                                       (marginalia--full-candidate cand))
                                      'integer))
      (marginalia--fields
       ((marginalia--file-owner attrs)
        :width 12 :face 'marginalia-file-owner)
       ((marginalia--file-modes attrs))
       ((+marginalia-file-size-colorful (file-attribute-size attrs))
        :width 7)
       ((+marginalia--time-colorful (file-attribute-modification-time attrs))
        :width 12))))

  (defun +marginalia--time-colorful (time)
    (let* ((seconds (float-time (time-subtract (current-time) time)))
           (color (doom-blend
                   (face-attribute 'marginalia-date :foreground nil t)
                   (face-attribute 'marginalia-documentation :foreground nil t)
                   (/ 1.0 (log (+ 3 (/ (+ 1 seconds) 345600.0)))))))
      ;; 1 - log(3 + 1/(days + 1)) % grey
      (propertize (marginalia--time time) 'face (list :foreground color))))

  (defun +marginalia-file-size-colorful (size)
    (let* ((size-index (/ (log10 (+ 1 size)) 7.0))
           (color (if (< size-index 10000000) ; 10m
                      (doom-blend 'orange 'green size-index)
                    (doom-blend 'red 'orange (- size-index 1)))))
      (propertize (file-size-human-readable size) 'face (list :foreground color)))))

(use-package! page-break-lines
  :commands page-break-lines-mode
  :init
  (autoload 'turn-on-page-break-lines-mode "page-break-lines")
  :config
  (setq page-break-lines-max-width fill-column)
  (map! :prefix "g"
        :desc "Prev page break" :nv "[" #'backward-page
        :desc "Next page break" :nv "]" #'forward-page))

(use-package! systemd
  :defer t)

(after! undo-tree
  (setq undo-tree-visualizer-diff t
        undo-tree-visualizer-timestamps t))

(after! text-mode
  (add-hook! 'text-mode-hook
    (unless (derived-mode-p 'org-mode)
      ;; Apply ANSI color codes
      (with-silent-modifications
        (ansi-color-apply-on-region (point-min) (point-max) t)))))

;; vertico enhancements
; inspired by: https://github.com/iamriel/doom.d
(after! vertico
  (defun posframe-poshandler-frame-below-center (info)
    "Posframe's position handler.

  Let posframe(0.5, 0.5) align to frame(0.5, 0.5).  The structure of
  INFO can be found in docstring of `posframe-show'."
    (cons (/ (- (plist-get info :parent-frame-width)
                (plist-get info :posframe-width))
             2)
          (+ (/ (- (plist-get info :parent-frame-height)
                (/ (plist-get info :posframe-height) 2))
             2) 100)))
  (require 'hotfuzz)
  (setq completion-styles '(hotfuzz orderless basic)
        completion-ignore-case t
        completion-category-defaults nil
        ; completion-category-overrides '((file (styles partial-completion)))
        read-buffer-completion-ignore-case t
        vertico-posframe-border-width 1
        vertico-posframe-poshandler #'posframe-poshandler-frame-below-center)
  (require 'vertico-quick)
  (map! :map vertico-map
        "M-q" #'vertico-quick-insert
        "C-q" #'vertico-quick-exit
        "M-h" #'vertico-directory-up
        [next] #'scroll-up-command
        [prior] #'scroll-down-command))


(after! dirvish
  (require 'dirvish-menu)
  (require 'dirvish-peek)
  (require 'dirvish-extras))


(use-package! elfeed
  :init
  (map! :leader
        "o e" #'elfeed)
  :config
  (setq elfeed-search-filter "@1-month-ago +unread -secondary"
        elfeed-search-clipboard-type 'CLIPBOARD
        elfeed-goodies/show-mode-padding 10
        elfeed-goodies/feed-source-column-width 24
        elfeed-goodies/tag-column-width 30)

  (map! :map elfeed-search-mode-map
        :n "g u" #'elfeed-update
        :n "J" #'elfeed-goodies/split-show-next
        :n "K" #'elfeed-goodies/split-show-prev)
  (map! :map elfeed-show-mode-map
        :n "J" #'elfeed-goodies/split-show-next
        :n "K" #'elfeed-goodies/split-show-prev)

  ;; from https://karthinks.com/software/declickbait-elfeed/
  (defun elfeed-declickbait-entry (entry)
    (let ((title (elfeed-entry-title entry)))
      (setf (elfeed-meta entry :title)
            (elfeed-title-transform title))))

  (defun elfeed-title-transform (title)
    "Declickbait string TITLE."
    (let* ((trim "\\(?:\\(?:\\.\\.\\.\\|[!?]\\)+\\)")
           (arr (split-string title nil t trim))
           (s-table (copy-syntax-table)))
      (modify-syntax-entry ?\' "w" s-table)
      (with-syntax-table s-table
        (mapconcat (lambda (word)
                     (cond
                      ((member word '("AND" "OR" "IF" "ON" "IT" "TO"
                                      "A" "OF" "VS" "IN" "FOR" "WAS"
                                      "IS" "BE"))
                       (downcase word))
                      ((member word '("WE" "DAY" "HOW" "WHY" "NOW" "OLD"
                                      "NEW" "MY" "TOO" "GOT" "GET" "THE"
                                      "ONE" "DO" "YOU"))
                       (capitalize word))
                      ((> (length word) 3) (capitalize word))
                      (t word)))
                   arr " "))))

  (add-hook 'elfeed-new-entry-hook #'elfeed-declickbait-entry))

(use-package! elfeed-tube
  :after elfeed
  :demand t
  :config
  ;; (setq elfeed-tube-auto-save-p nil) ;; t is auto-save (not default)
  ;; (setq elfeed-tube-auto-fetch-p t) ;;  t is auto-fetch (default)
  (setq elfeed-tube-fields '(duration thumbnail description chapters))

  (map! :map elfeed-search-mode-map
        :n "F" #'elfeed-tube-fetch
        :n [remap save-buffer] #'elfeed-tube-save)

  (elfeed-tube-setup))

(use-package! elfeed-summary
  :after elfeed
  :demand t
  :config
  (map! :map elfeed-search-mode-map
        :n "g S" #'elfeed-summary))

(after! browse-url
  (setq browse-url-browser-function 'eww-browse-url
        browse-url-secondary-browser-function 'browse-url-default-browser
        shr-max-image-proportion 0.6))

(after! eww
  ;; from https://protesilaos.com/emacs/dotemacs
  (defun prot-eww--rename-buffer ()
  "Rename EWW buffer using page title or URL.
  To be used by `eww-after-render-hook'."
    (let ((name (if (eq "" (plist-get eww-data :title))
                    (plist-get eww-data :url)
                  (plist-get eww-data :title))))
      (rename-buffer (format "*%s # eww*" name) t)))

  (add-hook 'eww-after-render-hook #'prot-eww--rename-buffer)
  (advice-add 'eww-back-url :after #'prot-eww--rename-buffer)
  (advice-add 'eww-forward-url :after #'prot-eww--rename-buffer))

(use-package! docker-compose-mode
  :defer t)

;; (after! comment-dwim-2
;;   (global-set-key (kbd "M-;") 'comment-dwim-2))

(use-package! comment-dwim-2
  :bind ([remap comment-dwim] . comment-dwim-2)
  :config (setq cd2/region-command 'cd2/comment-or-uncomment-region))

(after! rustic
  (use-package! cargo-transient
    :config
    (setq cargo-transient-compilation-buffer-name-function #'project-prefixed-buffer-name))
  (setq lsp-rust-analyzer-cargo-watch-command "clippy"
        lsp-rust-analyzer-server-display-inlay-hints t
        lsp-rust-analyzer-binding-mode-hints nil
        lsp-rust-analyzer-closing-brace-hints t
        lsp-rust-analyzer-display-closure-return-type-hints t
        lsp-rust-analyzer-display-chaining-hints t
        lsp-rust-analyzer-display-parameter-hints nil
        lsp-rust-analyzer-display-lifetime-elision-hints-enable "skip_trivial"
        lsp-rust-analyzer-display-lifetime-elision-hints-use-parameter-names nil
        lsp-rust-analyzer-display-reborrow-hints nil)  ;; this last one was problematic ..
  (map! :map rustic-mode-map
        :desc "LSP Code Action" "M-RET" #'lsp-execute-code-action
        :desc "LSP Imenu" "M-j" #'lsp-ui-imenu)
  (map! :map rustic-mode-map
        :localleader
        :desc "Cargo Transient" "c" #'cargo-transient
        :desc "Wrap/unwrap dbg!" "d" #'rust-dbg-wrap-or-unwrap
        :desc "Toggle inlay hints" "h" #'lsp-rust-analyzer-inlay-hints-mode
        :desc "Toggle mutability at point" "m" #'rust-toggle-mutability
        :desc "Promote module into directory" "p" #'rust-promote-module-into-dir))

(after! lsp-mode
  (setq lsp-headerline-breadcrumb-enable t
        lsp-headerline-breadcrumb-enable-symbol-numbers t
        lsp-idle-delay 0.6
        lsp-lens-enable nil
        lsp-signature-render-documentation nil
        lsp-completion-show-kind t
        lsp-eldoc-render-all t
        lsp-eldoc-enable-hover nil ;; annoying auto-popup
        lsp-ui-doc-enable nil
        lsp-ui-doc-header t
        lsp-ui-doc-show-with-cursor t
        lsp-ui-doc-include-signature t
        lsp-ui-doc-border (face-foreground 'default)
        lsp-ui-peek-always-show t
        lsp-ui-sideline-enable t
        lsp-ui-sideline-delay 0.2
        lsp-ui-sideline-show-code-actions t
        lsp-ui-sideline-show-diagnostics t
        lsp-ui-sideline-show-hover nil
        lsp-ui-sideline-show-symbol t)
  )

(after! company
  (setq company-selection-wrap-around t
        company-show-quick-access t
        company-insertion-on-trigger nil)
  )

;; from https://github.com/djs42012/doom-config/blob/master/config.org
(after! treemacs
  (setq +treemacs-git-mode 'extended))

;; cursor highlighting
(use-package! beacon
  :config
  (beacon-mode 1))

;; fancy titles (also tecosaur)
(setq frame-title-format
      '(""
        (:eval
         (if (s-contains-p org-roam-directory (or buffer-file-name ""))
             (replace-regexp-in-string
              ".*/[0-9]*-?" "☰ "
              (subst-char-in-string ?_ ?  buffer-file-name))
           "%b"))
        (:eval
         (let ((project-name (projectile-project-name)))
           (unless (string= "-" project-name)
             (format (if (buffer-modified-p)  " ◉ %s" "  ●  %s") project-name))))))

;; smart 'escape' key handling
;; https://github.com/seanfarley/dotfiles/blob/master/doom/%2Bbindings.el
(define-key key-translation-map (kbd "ESC") (kbd "C-g"))

;; because `read-char-exclusive' is a C function and doesn't read the remapping
;; above, we need to patch methods that call `read-char-exclusive' to call
;; `keyboard-quit'
(defadvice! quit-on-esc (orig-fn &rest args)
  :around '(org-export--dispatch-action)
  (cl-letf* ((old-read-char (symbol-function 'read-char-exclusive))
             ((symbol-function 'read-char-exclusive)
              (lambda (&optional prompt inherit-input-method seconds)
                (pcase (funcall old-read-char prompt inherit-input-method seconds)
                  (27 (keyboard-quit))
                  (x x)))))
    (apply orig-fn args)))

;; additional config (see tecosaur's 'better defaults')
(setq undo-limit 80000000
      auto-save-default t
      truncate-string-ellipsis "…"
      password-cache-expiry nil
      scroll-margin 2
      next-screen-context-lines 8
      fill-column 120

      confirm-kill-emacs nil
      global-auto-revert-non-file-buffers t

      calendar-week-start-day 1

      ;; This determines the style of line numbers in effect. If set to `nil', line
      ;; numbers are disabled. For relative line numbers, set this to `relative'.
      display-line-numbers-type t

      display-time-24hr-format t
      ;; display-time-day-and-date t
      display-time-load-average-threshold 5.1  ; hide the load average number

      ;; use the fish shell in /vterm/
      vterm-shell "/bin/fish")

;; Use C-tab & C-iso-lefttab to switch windows
(map! :nvi [C-tab] #'other-window)
(map! :nvi [C-<tab>] #'other-window)

(map! :nvi [C-iso-lefttab] (cmd! (other-window -1)))
(map! :nvi [C-<iso-lefttab>] (cmd! (other-window -1)))

(map! :nvi [C-S-tab] (cmd! (other-window -1)))
(map! :nvi [C-S-<tab>] (cmd! (other-window -1)))

;; from https://oremacs.com/2015/01/26/occur-dwim/
(defun occur-dwim ()
  "Call `occur' with a sane default."
  (interactive)
  (push (if (region-active-p)
            (buffer-substring-no-properties
             (region-beginning)
             (region-end))
          (let ((sym (thing-at-point 'symbol)))
            (when (stringp sym)
              (regexp-quote sym))))
        regexp-history)
  (call-interactively 'occur))

(global-set-key (kbd "M-s o") 'occur-dwim)

;; from https://christiantietze.de/posts/2022/09/delete-to-beginning-of-line-in-emacs-to-rewrite-code/
(defun ct/kill-to-beginning-of-line-dwim ()
  "Kill from point to beginning of line.

In `prog-mode', delete up to beginning of actual, not visual
line, stopping at whitespace. Repeat to delete whitespace. In
other modes, e.g. when editing prose, delete to beginning of
visual line only."
  (interactive)
  (let ((current-point (point)))
      (if (not (derived-mode-p 'prog-mode))
          ;; In prose editing, kill to beginning of (visual) line.
          (if visual-line-mode
              (kill-visual-line 0)
            (kill-line 0))
        ;; When there's whitespace at the beginning of the line, go to
        ;; before the first non-whitespace char.
        (beginning-of-line)
        (when (search-forward-regexp (rx (not space)) (point-at-eol) t)
          ;; `search-forward' puts point after the find, i.e. first
          ;; non-whitespace char. Step back to capture it, too.
          (backward-char))
        (kill-region (point) current-point))))

(global-set-key (kbd "M-<backspace>") #'ct/kill-to-beginning-of-line-dwim)

(after! yasnippet
  (yas-minor-mode-on))

(display-time-mode 1)
(global-subword-mode 1)
(global-auto-revert-mode 1)
(which-function-mode 1)

;; autosave when switching buffers/windows/frames
(defadvice switch-to-buffer (before save-buffer-now activate)
  (when buffer-file-name (save-buffer)))
(defadvice other-window (before other-window-now activate)
  (when buffer-file-name (save-buffer)))
(defadvice other-frame (before other-frame-now activate)
  (when buffer-file-name (save-buffer)))
(defadvice windmove-up (before other-window-now activate)
  (when buffer-file-name (save-buffer)))
(defadvice windmove-down (before other-window-now activate)
  (when buffer-file-name (save-buffer)))
(defadvice windmove-left (before other-window-now activate)
  (when buffer-file-name (save-buffer)))
(defadvice windmove-right (before other-window-now activate)
  (when buffer-file-name (save-buffer)))

(setq-default custom-file (expand-file-name ".custom.el" doom-private-dir))
(when (file-exists-p custom-file)
  (load custom-file))

;; modeline encoding hook
(defun doom-modeline-conditional-buffer-encoding ()
  "We expect the encoding to be LF UTF-8, so only show the modeline when this is not the case"
  (setq-local doom-modeline-buffer-encoding
              (unless (and (memq (plist-get (coding-system-plist buffer-file-coding-system) :category)
                                 '(coding-category-undecided coding-category-utf-8))
                           (not (memq (coding-system-eol-type buffer-file-coding-system) '(1 2))))
                t)))

(add-hook 'after-change-major-mode-hook #'doom-modeline-conditional-buffer-encoding)

;; more stuff
(require 'time-stamp)
(setq time-stamp-format "%Y-%02m-%02d %02H:%02M:%02S"
      time-stamp-line-limit 10)
(add-hook! 'before-save-hook 'time-stamp)

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
